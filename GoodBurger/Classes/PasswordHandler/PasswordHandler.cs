﻿using System;
using System.Security.Cryptography;
using System.Text;


namespace GoodBurger.Classes
{
    /// <summary>
    /// this class manages everything about passwords
    /// </summary>
    public static class PasswordHandler
    {
        /// <summary>
        /// Hash the string in parameter in MD5
        /// </summary>
        /// <param name="pwd">string</param>
        /// <returns>string</returns>
        public static string MD5Hash(string pwd)
        {

            StringBuilder hash = new StringBuilder();
            MD5CryptoServiceProvider md5provider = new MD5CryptoServiceProvider();

            byte[] bytes = md5provider.ComputeHash(new UTF8Encoding().GetBytes(pwd));

            for (int i = 0; i < bytes.Length; i++)
            {
                hash.Append(bytes[i].ToString("x2"));
            }
            return hash.ToString();

        }

    }
}