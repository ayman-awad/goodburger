namespace GoodBurger.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class first : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.AdministratorEntities",
                c => new
                    {
                        AdministratorId = c.Long(nullable: false, identity: true),
                        EmailAdmin = c.String(nullable: false),
                        PasswordAdmin = c.String(nullable: false),
                        Role = c.String(nullable: false),
                        FirstName = c.String(nullable: false),
                        LastName = c.String(nullable: false),
                        Phone = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.AdministratorId);
            
            CreateTable(
                "dbo.DirectorEntities",
                c => new
                    {
                        DirectorId = c.Long(nullable: false, identity: true),
                        FirstName = c.String(nullable: false),
                        LastName = c.String(nullable: false),
                        Phone = c.Int(nullable: false),
                        Email = c.String(nullable: false),
                        Password = c.String(nullable: false),
                        Address = c.String(nullable: false),
                        PostalCode = c.Int(nullable: false),
                        City = c.String(nullable: false),
                        DateRegister = c.DateTime(nullable: false),
                        CurrentAdministratorId = c.Long(nullable: false),
                        CurrentRestaurantId = c.Long(nullable: false),
                        Restaurant_RestaurantId = c.Long(),
                    })
                .PrimaryKey(t => t.DirectorId)
                .ForeignKey("dbo.AdministratorEntities", t => t.CurrentAdministratorId)
                .ForeignKey("dbo.RestaurantEntities", t => t.Restaurant_RestaurantId)
                .Index(t => t.CurrentAdministratorId)
                .Index(t => t.Restaurant_RestaurantId);
            
            CreateTable(
                "dbo.IngredientEntities",
                c => new
                    {
                        IngredientId = c.Long(nullable: false, identity: true),
                        Name = c.String(nullable: false),
                        TypeIngredient = c.String(nullable: false),
                        Quantity = c.Int(nullable: false),
                        CurrentDirectortId = c.Long(nullable: false),
                        CurrentRestaurantId = c.Long(nullable: false),
                        Director_DirectorId = c.Long(),
                        Restaurant_RestaurantId = c.Long(),
                    })
                .PrimaryKey(t => t.IngredientId)
                .ForeignKey("dbo.DirectorEntities", t => t.Director_DirectorId)
                .ForeignKey("dbo.RestaurantEntities", t => t.Restaurant_RestaurantId)
                .Index(t => t.Director_DirectorId)
                .Index(t => t.Restaurant_RestaurantId);
            
            CreateTable(
                "dbo.IngredientInBurgerEntities",
                c => new
                    {
                        BurgerId = c.Long(nullable: false),
                        IngredientId = c.Long(nullable: false),
                    })
                .PrimaryKey(t => new { t.BurgerId, t.IngredientId })
                .ForeignKey("dbo.IngredientEntities", t => t.IngredientId, cascadeDelete: true)
                .ForeignKey("dbo.BurgerEntities", t => t.BurgerId, cascadeDelete: true)
                .Index(t => t.BurgerId)
                .Index(t => t.IngredientId);
            
            CreateTable(
                "dbo.RestaurantEntities",
                c => new
                    {
                        RestaurantId = c.Long(nullable: false, identity: true),
                        Name = c.String(nullable: false),
                        Address = c.String(nullable: false),
                        PostalCode = c.Int(nullable: false),
                        Phone = c.Int(nullable: false),
                        Email = c.String(nullable: false),
                        HourMorning = c.String(nullable: false),
                        HourNight = c.String(nullable: false),
                        Open = c.Boolean(nullable: false),
                        ImageUrl = c.String(),
                        CurrentIdTown = c.Long(nullable: false),
                        CurrentAdminId = c.Long(nullable: false),
                    })
                .PrimaryKey(t => t.RestaurantId)
                .ForeignKey("dbo.AdministratorEntities", t => t.CurrentAdminId)
                .ForeignKey("dbo.TownEntities", t => t.CurrentIdTown)
                .Index(t => t.CurrentIdTown)
                .Index(t => t.CurrentAdminId);
            
            CreateTable(
                "dbo.DrinkEntities",
                c => new
                    {
                        DrinkId = c.Long(nullable: false, identity: true),
                        Name = c.String(nullable: false),
                        QuantityCentiliter = c.Int(nullable: false),
                        ImageUrl = c.String(),
                        Stock = c.Int(nullable: false),
                        Price = c.Single(nullable: false),
                        CurrentIdRestaurant = c.Long(nullable: false),
                    })
                .PrimaryKey(t => t.DrinkId)
                .ForeignKey("dbo.RestaurantEntities", t => t.CurrentIdRestaurant, cascadeDelete: true)
                .Index(t => t.CurrentIdRestaurant);
            
            CreateTable(
                "dbo.DrinksInOrdersEntities",
                c => new
                    {
                        DrinkId = c.Long(nullable: false),
                        OrderId = c.Long(nullable: false),
                        Quantity = c.Int(nullable: false),
                    })
                .PrimaryKey(t => new { t.DrinkId, t.OrderId })
                .ForeignKey("dbo.DrinkEntities", t => t.DrinkId, cascadeDelete: true)
                .ForeignKey("dbo.OrderEntities", t => t.OrderId, cascadeDelete: true)
                .Index(t => t.DrinkId)
                .Index(t => t.OrderId);
            
            CreateTable(
                "dbo.OrderEntities",
                c => new
                    {
                        OrderId = c.Long(nullable: false, identity: true),
                        DateOrder = c.DateTime(nullable: false),
                        SumOrder = c.Single(nullable: false),
                        CurrentUserId = c.Long(nullable: false),
                        CurrentRestaurantId = c.Long(nullable: false),
                        Restaurant_RestaurantId = c.Long(),
                    })
                .PrimaryKey(t => t.OrderId)
                .ForeignKey("dbo.RestaurantEntities", t => t.Restaurant_RestaurantId)
                .ForeignKey("dbo.UserEntities", t => t.CurrentUserId)
                .Index(t => t.CurrentUserId)
                .Index(t => t.Restaurant_RestaurantId);
            
            CreateTable(
                "dbo.BurgerInOrderEntities",
                c => new
                    {
                        BurgerId = c.Long(nullable: false),
                        OrderId = c.Long(nullable: false),
                        Quantity = c.Int(nullable: false),
                    })
                .PrimaryKey(t => new { t.BurgerId, t.OrderId })
                .ForeignKey("dbo.OrderEntities", t => t.OrderId, cascadeDelete: true)
                .ForeignKey("dbo.BurgerEntities", t => t.BurgerId, cascadeDelete: true)
                .Index(t => t.BurgerId)
                .Index(t => t.OrderId);
            
            CreateTable(
                "dbo.UserEntities",
                c => new
                    {
                        UserId = c.Long(nullable: false, identity: true),
                        FirstName = c.String(nullable: false),
                        LastName = c.String(nullable: false),
                        Phone = c.Int(nullable: false),
                        Email = c.String(nullable: false),
                        Password = c.String(nullable: false),
                        Address = c.String(nullable: false),
                        PostalCode = c.Int(nullable: false),
                        DateRegister = c.DateTime(nullable: false),
                        CreditCard = c.Int(nullable: false),
                        AccountBlock = c.Boolean(nullable: false),
                        CurrentTwonId = c.Long(nullable: false),
                    })
                .PrimaryKey(t => t.UserId)
                .ForeignKey("dbo.TownEntities", t => t.CurrentTwonId)
                .Index(t => t.CurrentTwonId);
            
            CreateTable(
                "dbo.BurgerLikedUserEntities",
                c => new
                    {
                        BurgerId = c.Long(nullable: false),
                        UserId = c.Long(nullable: false),
                    })
                .PrimaryKey(t => new { t.BurgerId, t.UserId })
                .ForeignKey("dbo.UserEntities", t => t.UserId, cascadeDelete: true)
                .ForeignKey("dbo.BurgerEntities", t => t.BurgerId, cascadeDelete: true)
                .Index(t => t.BurgerId)
                .Index(t => t.UserId);
            
            CreateTable(
                "dbo.TownEntities",
                c => new
                    {
                        TownId = c.Long(nullable: false, identity: true),
                        Country = c.String(nullable: false),
                        City = c.String(nullable: false),
                        ImageUrl = c.String(),
                        CurrentAdminId = c.Long(nullable: false),
                    })
                .PrimaryKey(t => t.TownId)
                .ForeignKey("dbo.AdministratorEntities", t => t.CurrentAdminId)
                .Index(t => t.CurrentAdminId);
            
            CreateTable(
                "dbo.BurgerEntities",
                c => new
                    {
                        BurgerId = c.Long(nullable: false, identity: true),
                        Name = c.String(nullable: false),
                        Stock = c.Int(nullable: false),
                        Price = c.Single(nullable: false),
                        ImageUrl = c.String(),
                        CurrentIdRestaurant = c.Long(nullable: false),
                    })
                .PrimaryKey(t => t.BurgerId)
                .ForeignKey("dbo.RestaurantEntities", t => t.CurrentIdRestaurant, cascadeDelete: true)
                .Index(t => t.CurrentIdRestaurant);
            
            CreateTable(
                "dbo.TagInBurgerEntities",
                c => new
                    {
                        BurgerId = c.Long(nullable: false),
                        TagId = c.Long(nullable: false),
                    })
                .PrimaryKey(t => new { t.BurgerId, t.TagId })
                .ForeignKey("dbo.BurgerEntities", t => t.BurgerId, cascadeDelete: true)
                .ForeignKey("dbo.TagEntities", t => t.TagId, cascadeDelete: true)
                .Index(t => t.BurgerId)
                .Index(t => t.TagId);
            
            CreateTable(
                "dbo.TagEntities",
                c => new
                    {
                        TagId = c.Long(nullable: false, identity: true),
                        Name = c.String(nullable: false),
                    })
                .PrimaryKey(t => t.TagId);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.TagInBurgerEntities", "TagId", "dbo.TagEntities");
            DropForeignKey("dbo.RestaurantEntities", "CurrentIdTown", "dbo.TownEntities");
            DropForeignKey("dbo.TagInBurgerEntities", "BurgerId", "dbo.BurgerEntities");
            DropForeignKey("dbo.BurgerEntities", "CurrentIdRestaurant", "dbo.RestaurantEntities");
            DropForeignKey("dbo.IngredientInBurgerEntities", "BurgerId", "dbo.BurgerEntities");
            DropForeignKey("dbo.BurgerLikedUserEntities", "BurgerId", "dbo.BurgerEntities");
            DropForeignKey("dbo.BurgerInOrderEntities", "BurgerId", "dbo.BurgerEntities");
            DropForeignKey("dbo.OrderEntities", "CurrentUserId", "dbo.UserEntities");
            DropForeignKey("dbo.UserEntities", "CurrentTwonId", "dbo.TownEntities");
            DropForeignKey("dbo.TownEntities", "CurrentAdminId", "dbo.AdministratorEntities");
            DropForeignKey("dbo.BurgerLikedUserEntities", "UserId", "dbo.UserEntities");
            DropForeignKey("dbo.OrderEntities", "Restaurant_RestaurantId", "dbo.RestaurantEntities");
            DropForeignKey("dbo.DrinksInOrdersEntities", "OrderId", "dbo.OrderEntities");
            DropForeignKey("dbo.BurgerInOrderEntities", "OrderId", "dbo.OrderEntities");
            DropForeignKey("dbo.IngredientEntities", "Restaurant_RestaurantId", "dbo.RestaurantEntities");
            DropForeignKey("dbo.DrinkEntities", "CurrentIdRestaurant", "dbo.RestaurantEntities");
            DropForeignKey("dbo.DrinksInOrdersEntities", "DrinkId", "dbo.DrinkEntities");
            DropForeignKey("dbo.DirectorEntities", "Restaurant_RestaurantId", "dbo.RestaurantEntities");
            DropForeignKey("dbo.RestaurantEntities", "CurrentAdminId", "dbo.AdministratorEntities");
            DropForeignKey("dbo.IngredientInBurgerEntities", "IngredientId", "dbo.IngredientEntities");
            DropForeignKey("dbo.IngredientEntities", "Director_DirectorId", "dbo.DirectorEntities");
            DropForeignKey("dbo.DirectorEntities", "CurrentAdministratorId", "dbo.AdministratorEntities");
            DropIndex("dbo.TagInBurgerEntities", new[] { "TagId" });
            DropIndex("dbo.TagInBurgerEntities", new[] { "BurgerId" });
            DropIndex("dbo.BurgerEntities", new[] { "CurrentIdRestaurant" });
            DropIndex("dbo.TownEntities", new[] { "CurrentAdminId" });
            DropIndex("dbo.BurgerLikedUserEntities", new[] { "UserId" });
            DropIndex("dbo.BurgerLikedUserEntities", new[] { "BurgerId" });
            DropIndex("dbo.UserEntities", new[] { "CurrentTwonId" });
            DropIndex("dbo.BurgerInOrderEntities", new[] { "OrderId" });
            DropIndex("dbo.BurgerInOrderEntities", new[] { "BurgerId" });
            DropIndex("dbo.OrderEntities", new[] { "Restaurant_RestaurantId" });
            DropIndex("dbo.OrderEntities", new[] { "CurrentUserId" });
            DropIndex("dbo.DrinksInOrdersEntities", new[] { "OrderId" });
            DropIndex("dbo.DrinksInOrdersEntities", new[] { "DrinkId" });
            DropIndex("dbo.DrinkEntities", new[] { "CurrentIdRestaurant" });
            DropIndex("dbo.RestaurantEntities", new[] { "CurrentAdminId" });
            DropIndex("dbo.RestaurantEntities", new[] { "CurrentIdTown" });
            DropIndex("dbo.IngredientInBurgerEntities", new[] { "IngredientId" });
            DropIndex("dbo.IngredientInBurgerEntities", new[] { "BurgerId" });
            DropIndex("dbo.IngredientEntities", new[] { "Restaurant_RestaurantId" });
            DropIndex("dbo.IngredientEntities", new[] { "Director_DirectorId" });
            DropIndex("dbo.DirectorEntities", new[] { "Restaurant_RestaurantId" });
            DropIndex("dbo.DirectorEntities", new[] { "CurrentAdministratorId" });
            DropTable("dbo.TagEntities");
            DropTable("dbo.TagInBurgerEntities");
            DropTable("dbo.BurgerEntities");
            DropTable("dbo.TownEntities");
            DropTable("dbo.BurgerLikedUserEntities");
            DropTable("dbo.UserEntities");
            DropTable("dbo.BurgerInOrderEntities");
            DropTable("dbo.OrderEntities");
            DropTable("dbo.DrinksInOrdersEntities");
            DropTable("dbo.DrinkEntities");
            DropTable("dbo.RestaurantEntities");
            DropTable("dbo.IngredientInBurgerEntities");
            DropTable("dbo.IngredientEntities");
            DropTable("dbo.DirectorEntities");
            DropTable("dbo.AdministratorEntities");
        }
    }
}
