﻿using GoodBurger.Models.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace GoodBurger.Models.IDAL
{
    interface ITagInBurgerDal : IDisposable
    {
        void Update(TagEntity Tag);

        void Remove(TagEntity Tag);

        TagEntity GetTagById(long id);

        List<TagEntity> GetAllTag();

        long Add(TagEntity Tag);

        String GetTagNameByIdTag(long idTag);
    }
}