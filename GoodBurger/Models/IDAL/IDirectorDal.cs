﻿using GoodBurger.Models.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace GoodBurger.Models.DAL.IDAL
{
    interface IDirectorDal : IDisposable
    {
        void Update(DirectorEntity owner);

        void Remove(DirectorEntity owner);

        DirectorEntity GetOwnerById(long id);

        DirectorEntity CheckIfOwnerExist(String email, String pwd);
   
        long Add(DirectorEntity owner);

        List<DirectorEntity> GetAllDirectors();
    
    }
      
}