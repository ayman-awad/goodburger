﻿using GoodBurger.Models.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GoodBurger.Models.DAL.IDAL
{
    interface IAdminDal :IDisposable
    {
        void Update(AdministratorEntity admin);

        void Remove(AdministratorEntity admin);

        AdministratorEntity GetAdminById(long id);

        AdministratorEntity CheckIfUserExist(String email, String pwd);

        List<AdministratorEntity> GetAllAdminUsers(long id);

        long Add(AdministratorEntity admin);

    }
}
