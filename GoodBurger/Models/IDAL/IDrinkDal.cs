﻿using GoodBurger.Models.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace GoodBurger.Models.DAL.IDAL
{
    interface IDrinkDal :IDisposable
    {
        void Update(DrinkEntity Drink);

        void Remove(DrinkEntity Drink);

        DrinkEntity GetDrinkById(long id);
        List<DrinkEntity> GetAllDrinksInRestaurant(long idRestaurant);
        long Add(DrinkEntity Drink);
        List<DrinkEntity> GetAllDrinks();
        void SubtractSale(DrinkEntity Drink);
    }
}