﻿using GoodBurger.Models.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace GoodBurger.Models.DAL.IDAL
{
    interface IBurgerDal : IDisposable
    {

        void Update(BurgerEntity Drink);

        void Remove(BurgerEntity Drink);

        void SubtractSale(BurgerEntity Burger);

        BurgerEntity GetBurgerById(long id);
        List<BurgerEntity> GetAllBurgerInRestaurant(long idRestaurant);
        long Add(BurgerEntity Burger);
        List<BurgerEntity> GetAllBurgers();
    }
}