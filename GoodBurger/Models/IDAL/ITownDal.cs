﻿using GoodBurger.Models.Entities;
using System;
using System.Collections.Generic;


namespace GoodBurger.Models.DAL.IDAL
{
    interface ITownDal : IDisposable
    {
        void UpdateTown(TownEntity t);

        long Add(TownEntity t);
        void RemoveTown(TownEntity t);

        String GetTownById(long id);

        List<TownEntity> GetAllTown();
        TownEntity GetAllInformationTownById(long id);
        String GetCityNameByIdTown(long id);

        long GetIdTownByNameCity(string nameCity);


    }
}
