﻿using GoodBurger.Models.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GoodBurger.Models.DAL.IDAL
{
    interface IUserDal : IDisposable
    {
        long RegisterUser(String firstname, String lastname, int phone, String email, String pwd,  String address, int postalCode, long towndid);

        void UpdateUser(UserEntity user);

        void RemoveUser(UserEntity user);

        UserEntity GetUserById(long id);

        UserEntity CheckIfUserExist(String email, String pwd);

        List<UserEntity> GetAllUsers();

    }
}
