﻿using GoodBurger.Models.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GoodBurger.Models.DAL.IDAL
{
    interface IIngredientDal : IDisposable
    {
        void Update(IngredientEntity ingredient);

        void Remove(IngredientEntity ingredient);

        IngredientEntity GetIngredientById(long id);

        long Add(IngredientEntity ingredient);

        List<IngredientEntity> GetAllIngredient();

        List<IngredientEntity> GetAllIngredientByIdRestaurant(long iddirector);

        String GetNameIngredientById(long idIngredient);
    }
}
