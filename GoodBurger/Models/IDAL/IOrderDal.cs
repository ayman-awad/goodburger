﻿using GoodBurger.Models.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GoodBurger.Models.DAL.IDAL
{
    interface IOrderDal : IDisposable
    {
        void Update(OrderEntity order);

        void Remove(OrderEntity order);


        long Add(OrderEntity order);

        List<OrderEntity> GetAllOrders();

        List<OrderEntity> GetAllOrderByIdRestaurant(long IdRestaurant);

        List<OrderEntity> GetAllorderByIduser(long IdUser);

        OrderEntity GetOrdderById(long IdOrder);

    }
}
