﻿using GoodBurger.Models.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace GoodBurger.Models.DAL.IDAL
{
    interface IRestaurantDal :IDisposable
    {
        long RegisterRestaurant(RestaurantEntity Restaurant);
         List<RestaurantEntity> GetAllRestaurant();


        List<RestaurantEntity> GetAllRestaurantByIdTown(long idTown);
        RestaurantEntity GetRestaurantById(long id);


        void Remove(RestaurantEntity restau);

        void Update(RestaurantEntity restaurant);
    }
}