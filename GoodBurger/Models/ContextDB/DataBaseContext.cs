﻿using GoodBurger.Models.Entities;
using System;
using System.Data.Entity;

namespace GoodBurger.Models.ContextDB
{
    public class DataBaseContext : DbContext
    {
        private const string _contextName = "GoodBurgerConnectionDB";
        private static string ContextName { get { return _contextName; } }

        public virtual DbSet<UserEntity> Users { get; set; }
        public virtual DbSet<DrinkEntity> Drinks { get; set; }
        public virtual DbSet<BurgerEntity> Burgers { get; set; }
        public virtual DbSet<IngredientEntity> Ingredients { get; set; }
        public virtual DbSet<OrderEntity> Orders { get; set; }
        public virtual DbSet<RestaurantEntity> Restaurants { get; set; }
        public virtual DbSet<TownEntity> Towns { get; set; }
        public virtual DbSet<AdministratorEntity> Administrators { get; set; }
        public virtual DbSet<DirectorEntity> Directors { get; set; }
        public virtual DbSet<TagEntity> Tags { get; set; }

        //DBSet for Many many relationships Entity
        public virtual DbSet<DrinksInOrdersEntity> DrinksInOrders { get; set; }
        public virtual DbSet<BurgerInOrderEntity> BurgersInOrders { get; set; }
        public virtual DbSet<BurgerLikedUserEntity> BurgersLikedUsers { get; set; }
        public virtual DbSet<IngredientInBurgerEntity> IngredientsInBurgers { get; set; }
        public virtual DbSet<TagInBurgerEntity> TagsInBurgers { get; set; }
       

   

        public DataBaseContext() : base(ContextName)
        {
            Database.SetInitializer(new CreateDatabaseIfNotExists<DataBaseContext>());
        }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {

            //one-to-many relationships Drink AND RESTAURANT
            modelBuilder.Entity<DrinkEntity>()
            .HasRequired<RestaurantEntity>(s => s.Restaurant)
            .WithMany(g => g.Drinks)
            .HasForeignKey<long>(s => s.CurrentIdRestaurant)
            .WillCascadeOnDelete(true);

            //one-to-many relationships USER AND TOWN
            modelBuilder.Entity<UserEntity>()
            .HasRequired<TownEntity>(s => s.Town)
            .WithMany(g => g.Users)
            .HasForeignKey<long>(s => s.CurrentTwonId)
            .WillCascadeOnDelete(false);

            //one-to-many relationships ORDER AND USER
            modelBuilder.Entity<OrderEntity>()
            .HasRequired<UserEntity>(s => s.User)
            .WithMany(g => g.Orders)
            .HasForeignKey<long>(s => s.CurrentUserId)
            .WillCascadeOnDelete(false);

            //one-to-many relationships BURGER AND RESTAURANT
            modelBuilder.Entity<BurgerEntity>()
            .HasRequired<RestaurantEntity>(s => s.Restaurant)
            .WithMany(g => g.RestaurantBurger)
            .HasForeignKey<long>(s => s.CurrentIdRestaurant)
            .WillCascadeOnDelete(true);

            //one-to-many relationships ADMIN AND TOWN
            modelBuilder.Entity<TownEntity>()
            .HasRequired<AdministratorEntity>(s => s.Administrator)
            .WithMany(g => g.TownsCreated)
            .HasForeignKey<long>(s => s.CurrentAdminId)
            .WillCascadeOnDelete(false);

            //one-to-many relationships ADMIN AND RESTAURANT
            modelBuilder.Entity<RestaurantEntity>()
            .HasRequired<AdministratorEntity>(s => s.Administrator)
            .WithMany(g => g.RestaurantCreated)
            .HasForeignKey<long>(s => s.CurrentAdminId)
            .WillCascadeOnDelete(false);

            //one-to-many relationships Directors AND Administrator
            modelBuilder.Entity<DirectorEntity>()
            .HasRequired<AdministratorEntity>(s => s.Administrator)
            .WithMany(g => g.Directors)
            .HasForeignKey<long>(s => s.CurrentAdministratorId)
            .WillCascadeOnDelete(false);

            //one-to-many relationships Restaurant AND Town
            modelBuilder.Entity<RestaurantEntity>()
            .HasRequired<TownEntity>(s => s.Town)
            .WithMany(g => g.RestaurantInTown)
            .HasForeignKey<long>(s => s.CurrentIdTown)
            .WillCascadeOnDelete(false);

            // relationship between DrinkEntity AND OrderEntity
            modelBuilder.Entity<DrinksInOrdersEntity>()
       .HasKey(c => new { c.DrinkId, c.OrderId });

            modelBuilder.Entity<DrinkEntity>()
                .HasMany(c => c.DrinksInOrders)
                .WithRequired()
                .HasForeignKey(c => c.DrinkId);

            modelBuilder.Entity<OrderEntity>()
                .HasMany(c => c.DrinksInOrders)
                .WithRequired()
                .HasForeignKey(c => c.OrderId);


            // relationship between BurgerEntity AND OrderEntity
            modelBuilder.Entity<BurgerInOrderEntity>()
       .HasKey(c => new { c.BurgerId, c.OrderId });

            modelBuilder.Entity<BurgerEntity>()
                .HasMany(c => c.BurgersInOrders)
                .WithRequired()
                .HasForeignKey(c => c.BurgerId);

            modelBuilder.Entity<OrderEntity>()
                .HasMany(c => c.BurgersInOrders)
                .WithRequired()
                .HasForeignKey(c => c.OrderId);

            // relationship between BurgerEntity AND UserEntity
            modelBuilder.Entity<BurgerLikedUserEntity>()
       .HasKey(c => new { c.BurgerId, c.UserId });

            modelBuilder.Entity<BurgerEntity>()
                .HasMany(c => c.BurgersLikedUsers)
                .WithRequired()
                .HasForeignKey(c => c.BurgerId);

            modelBuilder.Entity<UserEntity>()
                .HasMany(c => c.BurgersLikedUsers)
                .WithRequired()
                .HasForeignKey(c => c.UserId);


            // relationship between BurgerEntity AND IngredientEntity
            modelBuilder.Entity<IngredientInBurgerEntity>()
       .HasKey(c => new { c.BurgerId, c.IngredientId });

            modelBuilder.Entity<BurgerEntity>()
                .HasMany(c => c.IngredientsInBurgers)
                .WithRequired()
                .HasForeignKey(c => c.BurgerId);

            modelBuilder.Entity<IngredientEntity>()
                .HasMany(c => c.IngredientsInBurgers)
                .WithRequired()
                .HasForeignKey(c => c.IngredientId);


            // relationship between BurgerEntity AND TagEntity

            modelBuilder.Entity<TagInBurgerEntity>()
       .HasKey(c => new { c.BurgerId, c.TagId });

            modelBuilder.Entity<BurgerEntity>()
                .HasMany(c => c.TagsInBurgers)
                .WithRequired()
                .HasForeignKey(c => c.BurgerId);

            modelBuilder.Entity<TagEntity>()
                .HasMany(c => c.TagsInBurgers)
                .WithRequired()
                .HasForeignKey(c => c.TagId);


        }

    }
}