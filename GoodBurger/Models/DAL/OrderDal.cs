﻿using GoodBurger.Models.ContextDB;
using GoodBurger.Models.DAL.IDAL;
using GoodBurger.Models.Entities;
using System;
using System.Collections.Generic;
using System.Data.Entity.Migrations;
using System.Linq;
using System.Web;

namespace GoodBurger.Models.DAL
{
    public class OrderDal : IOrderDal
    {
        private DataBaseContext ContextDB = null;
   
        public OrderDal()
        {
            ContextDB = new DataBaseContext();
        }

        public long Add(OrderEntity order)
        {
            ContextDB.Orders.Add(order);
            ContextDB.SaveChanges();
            return order.OrderId;
        }

        public void Dispose()
        {
            ContextDB.Dispose();
        }

        public List<OrderEntity> GetAllOrderByIdRestaurant(long IdRestaurant)
        {
            List<OrderEntity> AllOrder = new List<OrderEntity>();
            AllOrder = ContextDB.Orders.Where(row => row.CurrentRestaurantId == IdRestaurant).ToList();
            return AllOrder;
        }

        public List<OrderEntity> GetAllorderByIduser(long IdUser)
        {
            List<OrderEntity> AllOrder = new List<OrderEntity>();
            AllOrder = ContextDB.Orders.Where(row => row.CurrentUserId == IdUser).ToList();
            return AllOrder;
        }

        public List<OrderEntity> GetAllOrders()
        {

            List<OrderEntity> AllOrder = new List<OrderEntity>();
            AllOrder = ContextDB.Orders.Select(row => row).ToList();
            return AllOrder;
        }

        public OrderEntity GetOrdderById(long IdOrder)
        {
            OrderEntity Order = new OrderEntity();
            Order = ContextDB.Orders.Where(row => row.OrderId == IdOrder).SingleOrDefault();
            return Order;
        }

        public void Remove(OrderEntity order)
        {
            ContextDB.Orders.Remove(order);
            ContextDB.SaveChanges();
        }

        public void Update(OrderEntity order)
        {
            ContextDB.Orders.AddOrUpdate(order);
            ContextDB.SaveChanges();
        }
    }
}