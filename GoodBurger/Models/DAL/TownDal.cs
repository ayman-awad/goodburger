﻿using GoodBurger.Models.ContextDB;
using GoodBurger.Models.DAL.IDAL;
using GoodBurger.Models.Entities;
using System;
using System.Collections.Generic;
using System.Data.Entity.Migrations;
using System.Linq;
using System.Web;

namespace GoodBurger.Models.DAL
{
    public class TownDal : ITownDal
    {

        private DataBaseContext ContextDB;
        private TownEntity town = null;

        public TownDal()
        {
            ContextDB = new DataBaseContext();
        }

        public long Add(TownEntity town)
        {
            ContextDB.Towns.Add(town);
            ContextDB.SaveChanges();
            return town.TownId;
        }

        public void Dispose()
        {
            ContextDB.Dispose();
        }

        public List<TownEntity> GetAllTown()
        {
            List<TownEntity> allTown = new List<TownEntity>();
            allTown = ContextDB.Towns.Select(row => row).ToList();
            return allTown;
        }

        public String GetTownById(long id)
        {
            town = ContextDB.Towns.Where(t => t.TownId == id).SingleOrDefault();
            return town.City;
        }

        public TownEntity GetAllInformationTownById(long id)
        {
            town = ContextDB.Towns.Where(t => t.TownId == id).SingleOrDefault();
            return town;
        }

        public void RemoveTown(TownEntity town)
        {
            ContextDB.Towns.Remove(town);
            ContextDB.SaveChanges();
        }

        public void UpdateTown(TownEntity town)
        {
            ContextDB.Towns.AddOrUpdate(town);
            ContextDB.SaveChanges();
        }

        public string GetCityNameByIdTown(long id)
        {
            town = ContextDB.Towns.Where(t => t.TownId == id).SingleOrDefault();
            return town.City;
        }

        public long GetIdTownByNameCity(string nameCity)
        {
            town = ContextDB.Towns.Where(t => t.City == nameCity).SingleOrDefault();
            return town.TownId;
        }
    }
}