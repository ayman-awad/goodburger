﻿using GoodBurger.Models.ContextDB;
using GoodBurger.Models.Entities;
using GoodBurger.Models.IDAL;
using System;
using System.Collections.Generic;
using System.Data.Entity.Migrations;
using System.Linq;
using System.Web;

namespace GoodBurger.Models.DAL
{
    public class TagDal : ITagInBurgerDal
    {

        private TagEntity Tag = new TagEntity();
        private TagInBurgerEntity TagBurger = new TagInBurgerEntity();
        private DataBaseContext ContextDB = null;
       

        public TagDal()
        {
            ContextDB = new DataBaseContext();
        }
        public long Add(TagEntity Tag)
        {
            ContextDB.Tags.Add(Tag);
            ContextDB.SaveChanges();
            return Tag.TagId;
        }

        public long Add(TagInBurgerEntity Tag)
        {
            throw new NotImplementedException();
        }

        public void Dispose()
        {
            ContextDB.Dispose();
        }

        public List<TagEntity> GetAllTag()
        {
            List<TagEntity> AllTag = new List<TagEntity>();
            AllTag = ContextDB.Tags.Select(row => row).ToList();
            return AllTag;
        }

        public TagEntity GetTagById(long id)
        {
            Tag = ContextDB.Tags.Where(row => row.TagId == id).Single();
            return Tag;
        }

        public string GetTagNameByIdTag(long idTag)
        {
            Tag = ContextDB.Tags.Where(row => row.TagId == idTag).SingleOrDefault();
            return Tag.Name;
        }

        public void Remove(TagEntity Tag)
        {
            ContextDB.Tags.Remove(Tag);
            ContextDB.SaveChanges();
        }

        public void Update(TagEntity Tag)
        {
            ContextDB.Tags.AddOrUpdate(Tag);
            ContextDB.SaveChanges();
        }

       
    }
}