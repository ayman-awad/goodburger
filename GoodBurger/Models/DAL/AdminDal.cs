﻿using GoodBurger.Models.ContextDB;
using GoodBurger.Models.DAL.IDAL;
using GoodBurger.Models.Entities;
using System.Collections.Generic;
using System.Data.Entity.Migrations;
using System.Linq;

namespace GoodBurger.Models.DAL
{
    public class AdminDal : IAdminDal
    {

        private DataBaseContext ContextDB = null;
        private AdministratorEntity admin = new AdministratorEntity();

        public AdminDal()
        {
            ContextDB = new DataBaseContext();
        }

        public List<AdministratorEntity> GetAllAdminUsers(long id)
        {
            List<AdministratorEntity> AllAdminUsers = new List<AdministratorEntity>();
            AllAdminUsers = ContextDB.Administrators.Where(row => row.UserID != id).ToList();
            return AllAdminUsers;
        }

        public AdministratorEntity CheckIfUserExist(string email, string pwd)
        {
            admin = ContextDB
                .Administrators
                .Where(e => e.Email == email)
                .Where(p => p.Password == pwd).SingleOrDefault();

            return admin;
        }

        public void Dispose()
        {
            ContextDB.Dispose();
        }

        public AdministratorEntity GetAdminById(long id)
        {
            admin = ContextDB.Administrators.Where(u => u.UserID == id).SingleOrDefault();
            return admin;
        }

        public void Remove(AdministratorEntity admin)
        {
            ContextDB.Administrators.Remove(admin);
            ContextDB.SaveChanges();
        }

        public void Update(AdministratorEntity admin)
        {
            ContextDB.Administrators.AddOrUpdate(admin);
            ContextDB.SaveChanges();
        }

        public long Add(AdministratorEntity admin)
        {
            ContextDB.Administrators.Add(admin);
            ContextDB.SaveChanges();
            return admin.UserID;
        }
    }
}