﻿using GoodBurger.Models.ContextDB;
using GoodBurger.Models.DAL.IDAL;
using GoodBurger.Models.Entities;
using System.Collections.Generic;
using System.Data.Entity.Migrations;
using System.Linq;

namespace GoodBurger.Models.DAL
{
    public class DirectorDal : IDirectorDal
    {
        private DataBaseContext ContextDB = null;
        private DirectorEntity Director = null;


        public DirectorDal()
        {
            ContextDB = new DataBaseContext();
        }
        public long Add(DirectorEntity Director)
        {
            ContextDB.Directors.Add(Director);
            ContextDB.SaveChanges();
            return Director.UserID;
        }

        public DirectorEntity CheckIfOwnerExist(string email, string pwd)
        {
            Director = ContextDB
                 .Directors
                 .Where(e => e.Email == email)
                 .Where(p => p.Password == pwd).SingleOrDefault();

            return Director;
        }

        public void Dispose()
        {
            ContextDB.Dispose();
        }

        public DirectorEntity GetOwnerById(long id)
        {
            Director = ContextDB.Directors.Where(u => u.UserID == id).SingleOrDefault();
            return Director;
        }


        public void Remove(DirectorEntity Director)
        {
            ContextDB.Directors.Remove(Director);
            ContextDB.SaveChanges();
        }

        public void Update(DirectorEntity Director)
        {
            ContextDB.Directors.AddOrUpdate(Director);
            ContextDB.SaveChanges();
        }

        public List<DirectorEntity> GetAllDirectors()
        {
            List<DirectorEntity> AllDirectors = new List<DirectorEntity>();
            AllDirectors = ContextDB.Directors.Select(row => row).ToList();
            return AllDirectors;
        }
    }
}