﻿using GoodBurger.Models.ContextDB;
using GoodBurger.Models.DAL.IDAL;
using GoodBurger.Models.Entities;
using System;
using System.Collections.Generic;
using System.Data.Entity.Migrations;
using System.Linq;
using System.Web;

namespace GoodBurger.Models.DAL
{
    public class DrinkDal : IDrinkDal
    {
        private DataBaseContext ContextDB = null;
        private DrinkEntity Drink = null;

        public long Add(DrinkEntity Drink)
        {
            ContextDB.Drinks.Add(Drink);
            ContextDB.SaveChanges();
            return Drink.DrinkId;
        }
        public DrinkDal()
        {
            ContextDB = new DataBaseContext();
        }
        public void Dispose()
        {
            ContextDB.Dispose();
        }

        public List<DrinkEntity> GetAllDrinks()
        {
            List<DrinkEntity> AllDrinks = new List<DrinkEntity>();
            AllDrinks = ContextDB.Drinks.Select(row => row).ToList();
            return AllDrinks;
        }

        public DrinkEntity GetDrinkById(long id)
        {
            Drink = ContextDB.Drinks.Where(u => u.DrinkId == id).SingleOrDefault();
            return Drink;
        }

        public void Remove(DrinkEntity Drink)
        {
            ContextDB.Drinks.Remove(Drink);
            ContextDB.SaveChanges();
        }

        public void Update(DrinkEntity Drink)
        {
            ContextDB.Drinks.AddOrUpdate(Drink);
            ContextDB.SaveChanges();
        }

        public List<DrinkEntity> GetAllDrinksInRestaurant(long idRestaurant)
        {
            List<DrinkEntity> AllDrinks = new List<DrinkEntity>();
            AllDrinks = ContextDB.Drinks.Where(u => u.CurrentIdRestaurant == idRestaurant).ToList();
            return AllDrinks;
        }

        public void SubtractSale(DrinkEntity Drink)
        {
            Drink.Stock -= 1;
            ContextDB.Drinks.AddOrUpdate(Drink);
            ContextDB.SaveChanges();
        }
    }
}