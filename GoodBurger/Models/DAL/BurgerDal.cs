﻿using GoodBurger.Models.ContextDB;
using GoodBurger.Models.DAL.IDAL;
using GoodBurger.Models.Entities;
using System;
using System.Collections.Generic;
using System.Data.Entity.Migrations;
using System.Linq;

namespace GoodBurger.Models.DAL
{
    public class BurgerDal : IBurgerDal
    {
        private DataBaseContext ContextDB = null;
        private BurgerEntity Burger = new BurgerEntity();



        public BurgerDal()
        {
            ContextDB = new DataBaseContext();
        }
        public long Add(BurgerEntity Burger)
        {
            ContextDB.Burgers.Add(Burger);
            ContextDB.SaveChanges();
            return Burger.BurgerId;
        }


        public void Dispose()
        {
            ContextDB.Dispose();
        }

        public List<BurgerEntity> GetAllBurgerInRestaurant(long idRestaurant)
        {
            List<BurgerEntity> AllBurgers = new List<BurgerEntity>();
            AllBurgers = ContextDB.Burgers.Where(row => row.CurrentIdRestaurant == idRestaurant).ToList();
            return AllBurgers;
        }

        public List<BurgerEntity> GetAllBurgers()
        {
            List<BurgerEntity> AllBurgers = new List<BurgerEntity>();
            AllBurgers = ContextDB.Burgers.Select(row => row).ToList();
            return AllBurgers;
        }

        public BurgerEntity GetBurgerById(long id)
        {
            Burger = ContextDB.Burgers.Where(u => u.BurgerId == id).SingleOrDefault();
            return Burger;
        }

        public void Remove(BurgerEntity Burger)
        {
            ContextDB.Burgers.Remove(Burger);
            ContextDB.SaveChanges();
        }

        public void SubtractSale(BurgerEntity Burger)
        {
            Burger.Stock -= 1;
            ContextDB.Burgers.AddOrUpdate(Burger);
            ContextDB.SaveChanges();
        }

        public void Update(BurgerEntity Burger)
        {
            ContextDB.Burgers.AddOrUpdate(Burger);
            ContextDB.SaveChanges();
        }
    }
}