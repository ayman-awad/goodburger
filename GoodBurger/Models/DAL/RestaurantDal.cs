﻿using GoodBurger.Models.ContextDB;
using GoodBurger.Models.DAL.IDAL;
using GoodBurger.Models.Entities;
using System;
using System.Collections.Generic;
using System.Data.Entity.Migrations;
using System.Data.Entity.Validation;
using System.Linq;
using System.Web;

namespace GoodBurger.Models.DAL
{
    public class RestaurantDal : IRestaurantDal
    {
        private DataBaseContext ContextDB = null;
        private RestaurantEntity resto = null;

        public RestaurantDal()
        {
            ContextDB = new DataBaseContext();
        }
        public void Dispose()
        {
            ContextDB.Dispose();
        }

        public List<RestaurantEntity> GetAllRestaurant()
        {
            List<RestaurantEntity> allRestaurant = new List<RestaurantEntity>();
            allRestaurant = ContextDB.Restaurants.Select(row => row).ToList();
            return allRestaurant;
        }

        public List<RestaurantEntity> GetAllRestaurantByIdTown(long idTown)
        {
            List<RestaurantEntity> allRestaurant = new List<RestaurantEntity>();
            allRestaurant = ContextDB.Restaurants.Where(row => row.CurrentIdTown == idTown).ToList();
            return allRestaurant;
        }

        public RestaurantEntity GetRestaurantById(long id)
        {
            resto = ContextDB.Restaurants.Where(r => r.RestaurantId == id).SingleOrDefault();
            return resto;
        }


        public long RegisterRestaurant(RestaurantEntity Restaurant)
        {
            ContextDB.Restaurants.Add(Restaurant);
            ContextDB.SaveChanges();
            return Restaurant.RestaurantId;
        }

        public void Remove(RestaurantEntity restau)
        {
            ContextDB.Restaurants.Remove(restau);
            ContextDB.SaveChanges();
        }

        public void Update(RestaurantEntity restaurant)
        {
            ContextDB.Restaurants.AddOrUpdate(restaurant);
            ContextDB.SaveChanges();
        }
    }
}