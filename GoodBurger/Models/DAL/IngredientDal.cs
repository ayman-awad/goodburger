﻿using GoodBurger.Models.ContextDB;
using GoodBurger.Models.DAL.IDAL;
using GoodBurger.Models.Entities;
using System;
using System.Collections.Generic;
using System.Data.Entity.Migrations;
using System.Linq;

namespace GoodBurger.Models.DAL
{

    public class IngredientDal : IIngredientDal
    {
        private DataBaseContext ContextDB = null;
        private IngredientEntity Ingredient = null;


        public IngredientDal()
        {
            ContextDB = new DataBaseContext();
        }

        public long Add(IngredientEntity ingredient)
        {
            ContextDB.Ingredients.Add(ingredient);
            ContextDB.SaveChanges();
            return ingredient.IngredientId;
        }

        public void Dispose()
        {
            ContextDB.Dispose();
        }

        public List<IngredientEntity> GetAllIngredient()
        {
            List<IngredientEntity> AllIngredients = new List<IngredientEntity>();
            AllIngredients = ContextDB.Ingredients.Select(row => row).ToList();
            return AllIngredients;
        }

        public List<IngredientEntity> GetAllIngredientByIdRestaurant(long idRestaurat)
        {
            List<IngredientEntity> AllIngredients = new List<IngredientEntity>();
            AllIngredients = ContextDB.Ingredients.Where(row => row.CurrentRestaurantId == idRestaurat).ToList();
            return AllIngredients;
        }

        public IngredientEntity GetIngredientById(long id)
        {
            Ingredient = ContextDB.Ingredients.Where(u => u.IngredientId == id).SingleOrDefault();
            return Ingredient;
        }

        public string GetNameIngredientById(long idIngredient)
        {
            Ingredient = ContextDB.Ingredients.Where(row => row.IngredientId == idIngredient).SingleOrDefault();
            return Ingredient.Name;
        }

        public void Remove(IngredientEntity ingredient)
        {
            ContextDB.Ingredients.Remove(ingredient);
            ContextDB.SaveChanges();
        }

        public void Update(IngredientEntity ingredient)
        {
            ContextDB.Ingredients.AddOrUpdate(ingredient);
            ContextDB.SaveChanges();
        }
    }
}