﻿using GoodBurger.Models.ContextDB;
using GoodBurger.Models.DAL.IDAL;
using GoodBurger.Models.Entities;
using System;
using System.Collections.Generic;
using System.Data.Entity.Migrations;
using System.Linq;
using System.Web;

namespace GoodBurger.Models.DAL
{
    public class UserDal : IUserDal
    {
        private DataBaseContext ContextDB;

        private UserEntity user = null;

        public UserDal()
        {
            ContextDB = new DataBaseContext();
        }
        public UserEntity CheckIfUserExist(string email, string pwd)
        {
            user = ContextDB
                 .Users
                 .Where(e => e.Email == email)
                 .Where(p => p.Password == pwd).SingleOrDefault();

            return user;
        }

        public void Dispose()
        {
            ContextDB.Dispose();
        }

        public UserEntity GetUserById(long id)
        {
            user = ContextDB.Users.Where(u => u.UserID == id).SingleOrDefault();
            return user;
        }
        public long RegisterUser(string firstname, string lastname, int phone, string email, string pwd, string address, int postalCode, long towndid)
        {
            UserEntity user = new UserEntity(firstname, lastname, phone, email, pwd, address, postalCode, towndid);
            ContextDB.Users.Add(user);
            ContextDB.SaveChanges();
            return user.UserID;
        }
        public Boolean PasswordIsValide(string passwordSubmit)
        {
            bool IsValid = false;

            user  = ContextDB.Users.Where(p => p.Password == passwordSubmit).SingleOrDefault();
            if(user != null) { 
                 
                return IsValid = true;
            }

            return IsValid;
        }
        public void RemoveUser(UserEntity user)
        {
            ContextDB.Users.Remove(user);
            ContextDB.SaveChanges();
        }
        public void UpdateUser(UserEntity user)
        {
            ContextDB.Users.AddOrUpdate(user);
            ContextDB.SaveChanges();
        }
        public List<UserEntity> GetAllUsers()
        {
            List<UserEntity> allUsers = new List<UserEntity>();
            allUsers = ContextDB.Users.Select(row => row).ToList();
            return allUsers;
        }
    }
}