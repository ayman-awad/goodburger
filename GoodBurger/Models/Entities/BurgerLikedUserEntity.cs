﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace GoodBurger.Models.Entities
{
    public class BurgerLikedUserEntity
    {
        public long BurgerId { get; set; }
        public long UserId { get; set; }
    
        public BurgerLikedUserEntity() { }
        public BurgerLikedUserEntity(long burgerid,long userid) {
            this.BurgerId = burgerid;
            this.UserId = userid;
        }

    }
}