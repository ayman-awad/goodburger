﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace GoodBurger.Models.Entities
{
    public class BurgerEntity
    {
        [Key]
        public long BurgerId { get; set; }

        [Required]
        [DataType(DataType.Text)]
        public string Name { get; set; }

        [Required]
        public int Stock { get; set; }

        [Required]
        public float Price { get; set; }

        [DataType(DataType.ImageUrl)]
        public String ImageUrl { get; set; }

        //Table relotionships
        public virtual ICollection<BurgerLikedUserEntity> BurgersLikedUsers { get; set; }
        public virtual ICollection<TagInBurgerEntity> TagsInBurgers { get; set; }
        public virtual ICollection<BurgerInOrderEntity> BurgersInOrders { get; set; }
        public virtual ICollection<IngredientInBurgerEntity> IngredientsInBurgers { get; set; }
        public virtual long CurrentIdRestaurant { get; set; }
        public virtual RestaurantEntity Restaurant { get; set; }

        public BurgerEntity()
        {
        }

        public BurgerEntity(string name,int stock ,float price,string img,long idRestaurant){
            this.Name = name;
            this.Stock = stock;
            this.Price = price;
            this.ImageUrl = img;
            this.CurrentIdRestaurant = idRestaurant;
            this.BurgersLikedUsers = new List<BurgerLikedUserEntity>();
            this.BurgersInOrders = new List<BurgerInOrderEntity>();
            this.IngredientsInBurgers = new List<IngredientInBurgerEntity>();
            }

    
    }
}