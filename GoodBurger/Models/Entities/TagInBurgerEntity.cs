﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace GoodBurger.Models.Entities
{
    public class TagInBurgerEntity
    {
        
        public long TagId { get; set; }
        public long BurgerId { get; set; }

        public TagInBurgerEntity() { }

        public TagInBurgerEntity(long tagid, long burgerid)
        {
            this.TagId = tagid;
            this.BurgerId = burgerid;
        }
    }
}