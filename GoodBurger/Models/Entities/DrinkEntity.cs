﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace GoodBurger.Models.Entities
{
    public class DrinkEntity
    {
        [Key]
        public long DrinkId { get; set; }

        [Required]
        public String Name { get; set; }

        [Required]
        public int QuantityCentiliter { get; set; }

        [DataType(DataType.ImageUrl)]
        public String ImageUrl { get; set; }

        [Required]
        public int Stock { get; set; }

        [Required]
        public float Price { get; set; }

        //relationships between Entities
        public virtual ICollection<DrinksInOrdersEntity> DrinksInOrders { get; set; }

        public long CurrentIdRestaurant { get; set; }
        public virtual RestaurantEntity Restaurant { get; set; }


        public DrinkEntity() { }

        public DrinkEntity(string name,int quantitycentiliter, string img,int stock,long idrestaurant,float price) {

            this.Name = name;
            this.QuantityCentiliter = quantitycentiliter;
            this.ImageUrl = img;
            this.Stock = stock;
            this.CurrentIdRestaurant = idrestaurant;
            this.Price = price;
            this.DrinksInOrders = new List<DrinksInOrdersEntity>();
         
        }
    }



}