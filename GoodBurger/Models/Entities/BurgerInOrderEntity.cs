﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace GoodBurger.Models.Entities
{
    public class BurgerInOrderEntity
    {
        public long BurgerId { get; set; }
        public long OrderId { get; set; }
        public int Quantity { get; set; }

        public BurgerInOrderEntity() { }
        public BurgerInOrderEntity(long burgerid,long orderid,int qt) {
            this.BurgerId = burgerid;
            this.OrderId = orderid;
            this.Quantity = qt;
        }
    }
}