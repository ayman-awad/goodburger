﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace GoodBurger.Models.Entities
{
    public class IngredientEntity
    {
        [Key]
        public long IngredientId { get; set; }

        [Required]
        public String Name { get; set; }

        [Required]
        public String TypeIngredient { get; set; }

        [Required]
        public int Quantity { get; set; }

        //relationships between Entities
        public virtual ICollection<IngredientInBurgerEntity> IngredientsInBurgers { get; set; }

        public long CurrentDirectortId { get; set; }
        public virtual DirectorEntity Director { get; set; }

        public long CurrentRestaurantId { get; set; }
        public virtual RestaurantEntity Restaurant { get; set; }

        public IngredientEntity(){}

        public IngredientEntity(String name,String typeingredient, int quantity,long idDirector,long restuarantid)
        {
            this.Name = name;
            this.TypeIngredient = typeingredient;
            this.Quantity = quantity;
            this.CurrentDirectortId = idDirector;
            this.CurrentRestaurantId = restuarantid;
            this.IngredientsInBurgers = new List<IngredientInBurgerEntity>();
        }

    }
}