﻿using GoodBurger.Classes.User;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace GoodBurger.Models.Entities
{
    public class AdministratorEntity : User
    {
       
        //relationships between Entities
        public virtual ICollection<TownEntity> TownsCreated { get; set; }
        public virtual ICollection<RestaurantEntity> RestaurantCreated { get; set; }
        public virtual ICollection<DirectorEntity> Directors { get; set; }

        public AdministratorEntity() { }

        public AdministratorEntity(string email, string pwd, string role,string firstname,string lastname, int phone)
        {
            this.Email = email;
            this.Password = pwd;
            this.Role = role;
            this.FirstName = firstname;
            this.LastName = lastname;
            this.Phone = phone;
            this.TownsCreated = new List<TownEntity>();
            this.RestaurantCreated = new List<RestaurantEntity>();
            this.Directors = new List<DirectorEntity>();
        }
    }
}