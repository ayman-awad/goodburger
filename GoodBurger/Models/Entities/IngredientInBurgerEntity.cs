﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace GoodBurger.Models.Entities
{
    public class IngredientInBurgerEntity
    {
        public long IngredientId { get; set; }
        public long BurgerId { get; set; }

        public IngredientInBurgerEntity() { }

        public IngredientInBurgerEntity(long ingredientid,long burgerid) {
            this.IngredientId = ingredientid;
            this.BurgerId = burgerid;
        }
    }
}