﻿using GoodBurger.Classes.User;
using GoodBurger.Models.DAL;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Web;

namespace GoodBurger.Models.Entities
{
   
    public class UserEntity : User
    {

        private static UserDal dal = new UserDal();

        [DataType(DataType.CreditCard)]
        public int CreditCard { get; set; }

        public Boolean AccountBlock { get; set; }

        //relationships between Entities
        public long CurrentTwonId { get; set; }
        public virtual TownEntity Town { get; set; }
        public virtual ICollection<BurgerLikedUserEntity> BurgersLikedUsers { get; set; }
        public virtual ICollection<OrderEntity> Orders { get; set; }

        public UserEntity() {}
        public UserEntity(String firstname, String lastname, int phone,String email,String password,String address, int postalcode,long townid )
        {
            this.FirstName = firstname;
            this.LastName = lastname;
            this.Phone = phone;
            this.Email = email;
            this.Password = password;
            this.Address = address;
            this.PostalCode = postalcode;
            this.DateRegister = DateTime.Now;
            this.CreditCard = 0;
            this.AccountBlock = false;
            this.CurrentTwonId = townid;
            this.BurgersLikedUsers = new List<BurgerLikedUserEntity>();
            this.Orders = new List<OrderEntity>();
        }

    
        

    }
}