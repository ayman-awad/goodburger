﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace GoodBurger.Models.Entities
{
    public class DrinksInOrdersEntity
    {
        public long DrinkId { get; set; }
        public long OrderId { get; set; }

        public int Quantity { get; set; }
        public DrinksInOrdersEntity() { }

        public DrinksInOrdersEntity(long drinkid,long orderid,int qt) {
            this.DrinkId = drinkid;
            this.OrderId = orderid;
            this.Quantity = qt;
        }
    }
}