﻿using GoodBurger.Classes.User;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace GoodBurger.Models.Entities
{
    public class DirectorEntity : User
    {

        //relationships between Entities
        public long CurrentAdministratorId { get; set; }
        public virtual AdministratorEntity Administrator { get; set; }

        public long CurrentRestaurantId { get; set; }
        public virtual RestaurantEntity Restaurant { get; set; }

        public virtual ICollection<IngredientEntity> Ingredients { get; set; }

        public DirectorEntity() { }

        public DirectorEntity(string firstname, string lastname, int phone, string email, string pwd, string address, int postalcode, string city, long id,long idrestaurant)
        {
            this.FirstName = firstname;
            this.LastName = lastname;
            this.Phone = phone;
            this.Email = email;
            this.Password = pwd;
            this.Address = address;
            this.PostalCode = postalcode;
            this.City = city;
            this.DateRegister = DateTime.Now;
            this.CurrentAdministratorId = id;
            this.CurrentRestaurantId = idrestaurant;
        }
    }
}
