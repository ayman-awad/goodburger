﻿using GoodBurger.Models.DAL;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace GoodBurger.Models.Entities
{
    public class TownEntity
    {
        
        [Key]
        public long TownId { get; set; }

        [Required]
        [DataType(DataType.Text)]
        public String Country { get; set; }

        [Required]
        [DataType(DataType.Text)]
        public String City { get; set; }

        [DataType(DataType.ImageUrl)]
        public String ImageUrl { get; set; }

        //relationships between Entities
        public virtual ICollection<UserEntity> Users { get; set; }
        public virtual ICollection<RestaurantEntity> RestaurantInTown { get; set; }
        public long CurrentAdminId { get; set; }
        public virtual AdministratorEntity Administrator { get; set; }

        public TownEntity() { }

        public TownEntity(String city,String country, long idadmin) {
            this.City = city;
            this.Country = country;
            this.CurrentAdminId = idadmin;
            this.Users = new List<UserEntity>();
            this.RestaurantInTown = new List<RestaurantEntity>();
        }


        
    }
}