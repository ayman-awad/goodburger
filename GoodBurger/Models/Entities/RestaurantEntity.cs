﻿using GoodBurger.Models.DAL;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace GoodBurger.Models.Entities
{
    public class RestaurantEntity
    {
        [Key]
        public long RestaurantId { get; set; }

        [Required]
        public String Name { get; set; }

        [Required]
        public String Address { get; set; }

        [Required]
        [DataType(DataType.PostalCode)]
        public int PostalCode { get; set; }

        [Required]
        [DataType(DataType.PhoneNumber)]
        public int Phone { get; set; }

        [Required]
        [DataType(DataType.EmailAddress)]
        public String Email { get; set; }

        [Required]
        public String HourMorning { get; set; }

        [Required]
        public String HourNight { get; set; }

        public Boolean Open { get; set; }

        [DataType(DataType.ImageUrl)]
        public String ImageUrl { get; set; }

        //relationships between Entities
        public virtual ICollection<DrinkEntity> Drinks { get; set; }
        public virtual ICollection<BurgerEntity> RestaurantBurger { get; set; }
        public virtual ICollection<DirectorEntity> Directors { get; set; }
        public virtual ICollection<IngredientEntity> Ingredients { get; set; }
        public virtual ICollection<OrderEntity> Orders { get; set; }
        public long CurrentIdTown { get; set; }
        public virtual TownEntity Town { get; set; }
        
        public long CurrentAdminId { get; set; }
        public virtual AdministratorEntity Administrator { get; set; }
     

        public RestaurantEntity(){}


        public RestaurantEntity(String name, String address, int postalcode, int phone, String email, String hourmorning, String hournight, long townid, long adminid)
        {
            this.Name = name;
            this.Address = address;
            this.PostalCode = postalcode;
            this.Phone = phone;
            this.Email = email;
            this.Open = false;
            this.ImageUrl = null;
            this.HourMorning = hourmorning;
            this.HourNight = hournight;
            this.CurrentIdTown = townid;
            this.CurrentAdminId = adminid;
        }

     

       

    }
}