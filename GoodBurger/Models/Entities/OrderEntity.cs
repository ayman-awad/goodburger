﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace GoodBurger.Models.Entities
{
    public class OrderEntity
    {
        [Key]
        public long OrderId { get; set; }

        [Required]
        [DataType(DataType.DateTime)]
        [DisplayFormat(ApplyFormatInEditMode = true, DataFormatString = "{0:yyyy/MM/dd HH:mm:ss}")]
        public DateTime DateOrder { get; set; }


        public float SumOrder { get; set; }

        //one-to-many
        public long CurrentUserId { get; set; }
        public virtual UserEntity User { get; set; }
        public long CurrentRestaurantId { get; set; }
        public virtual RestaurantEntity Restaurant { get; set; }

        //relationships between Entities
        public virtual ICollection<BurgerInOrderEntity> BurgersInOrders { get; set; }
        public virtual ICollection<DrinksInOrdersEntity> DrinksInOrders { get; set; }


        public OrderEntity() { }
        public OrderEntity(DateTime date,long iduser,long idrestaurant, float sum)
        {
            this.DateOrder = DateTime.Now;
            this.CurrentUserId = iduser;
            this.CurrentRestaurantId = idrestaurant;
            this.SumOrder = sum;
            this.BurgersInOrders = new List<BurgerInOrderEntity>();
            this.DrinksInOrders = new List<DrinksInOrdersEntity>();
        }
    }
}