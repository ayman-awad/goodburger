﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace GoodBurger.Models.Entities
{
    public class TagEntity
    {
        [Key]
        public long TagId {get;set;}

        [Required]
        [DataType(DataType.Text)]
        public String Name { get; set; }

        public TagEntity(){}

        public TagEntity(string name) {
            this.Name = name;
        }
        public virtual ICollection<TagInBurgerEntity> TagsInBurgers { get; set; }
    }

}