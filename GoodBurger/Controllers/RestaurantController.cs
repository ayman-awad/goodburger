﻿using GoodBurger.Models.ContextDB;
using GoodBurger.Models.DAL;
using GoodBurger.Models.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace GoodBurger.Controllers
{
    [Authorize]
    public class RestaurantController : Controller
    {
        private RestaurantDal DalRestaurant = new RestaurantDal();
        private DirectorDal DalDirector = new DirectorDal();
        private TownDal DalTown = new TownDal();
        private BurgerDal DalBurger = new BurgerDal();
        private DrinkDal DalDrink = new DrinkDal();
        private TagDal DalTag = new TagDal();
        private RestaurantEntity Restaurant = new RestaurantEntity();
        private DirectorEntity Director = new DirectorEntity();
        private BurgerEntity Burger = new BurgerEntity();

        /// <summary>
        /// Get index view
        /// </summary>
        /// <returns>View</returns>
        // GET: Restaurant
        [HttpGet]
        public ActionResult Index()
        {
        
            List<RestaurantEntity> AllRestaurant = new List<RestaurantEntity>();
            AllRestaurant = DalRestaurant.GetAllRestaurant();
            ViewData["AllRestaurant"] = AllRestaurant;
            return View();
        }

        /// <summary>
        /// Get form to add restaurant in data base
        /// </summary>
        /// <returns>View</returns>
        // Get: ADD Restaurant
        [HttpGet]
        public ActionResult FormRestaurant()
        {
            List<TownEntity> AllTown = DalTown.GetAllTown();
            ViewData["AllTown"] = AllTown;
            using (DataBaseContext ContextDB = new DataBaseContext())
            {
                
                List<TagEntity> AllTags = new List<TagEntity>();
                AllTags = ContextDB.Tags.Select(row => row).ToList();
                ViewData["AllTags"] = AllTags;
            }
            return View();
        }

        /// <summary>
        /// check and add restaurant in data base 
        /// </summary>
        /// <param name="dataForm">FormCollection</param>
        /// <returns>RedirectToAction</returns>
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Add(FormCollection dataForm)
        {
            string CurrentCookie = HttpContext.User.Identity.Name;
            long idAdmin = long.Parse(CurrentCookie);
          
            string name = dataForm["Name"];
            string email = dataForm["Email"];
            int phone = int.Parse(dataForm["Phone"]);
            string address = dataForm["Address"];
            int postal = int.Parse(dataForm["PostalCode"]);
            long townid = long.Parse(dataForm["Town"]);
            string hourMorning = dataForm["HourMorning"];
            string hournight = dataForm["HourNight"];
         

            Restaurant = new RestaurantEntity(name, address, postal, phone, email, hourMorning, hournight,  townid, idAdmin);

            long IdResto = DalRestaurant.RegisterRestaurant(Restaurant);

           
           if (!IdResto.Equals(0))
           {
                return RedirectToAction("Index", "Restaurant");
            }
            return RedirectToAction("HomeAdmin", "Admin");
        }

        /// <summary>
        /// Remove restaurant fro mdata base
        /// </summary>
        /// <param name="idRestaurant">long</param>
        /// <returns>RedirectToAction</returns>
        [HttpGet]
        public ActionResult Remove(long idRestaurant)
        {
            RestaurantEntity restau = DalRestaurant.GetRestaurantById(idRestaurant);
            DalRestaurant.Remove(restau);
            return RedirectToAction("Index", "Restaurant");
        }

        /// <summary>
        /// Get form update restaurant
        /// </summary>
        /// <param name="idRestaurant">long</param>
        /// <returns>View</returns>
        [HttpGet]
        public ActionResult FormUpdate(long idRestaurant)
        {
            List<TownEntity> AllTown = DalTown.GetAllTown();
            ViewData["AllTown"] = AllTown;
            RestaurantEntity restau = DalRestaurant.GetRestaurantById(idRestaurant);

            return View(restau);
        }

        /// <summary>
        /// check and update restaurant in data base
        /// </summary>
        /// <param name="dataForm">FormCollection</param>
        /// <param name="idRestaurant">long</param>
        /// <returns>RedirectToAction</returns>
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Update( FormCollection dataForm,long idRestaurant)
        {
            RestaurantEntity restau = DalRestaurant.GetRestaurantById(idRestaurant);
    

            if (dataForm != null)
            {
                Boolean Isdifferent = false;
                string name = dataForm["Name"];
                int phone = int.Parse(dataForm["Phone"]);
                string email = dataForm["Email"];
                string address = dataForm["Address"];
                int postalCode = int.Parse(dataForm["PostalCode"]);
                long townid = long.Parse(dataForm["Town"]);
                string hourMorning = dataForm["HourMorning"];
                string hournight = dataForm["HourNight"];

                if (restau != null)
                {

                    if (name != restau.Name)
                    {
                        restau.Name = name;
                        Isdifferent = true;
                    }
                    if (phone != restau.Phone)
                    {
                        restau.Phone = phone;
                        Isdifferent = true;
                    }
                    if (email != restau.Email)
                    {
                        restau.Email = email;
                        Isdifferent = true;
                    }
               
                    if (phone != restau.Phone)
                    {
                        restau.Phone = phone;
                        Isdifferent = true;
                    }
                 
                    if (postalCode != restau.PostalCode)
                    {
                        restau.PostalCode = postalCode;
                        Isdifferent = true;
                    }

                    if (townid != restau.CurrentIdTown)
                    {
                        restau.CurrentIdTown = townid;
                        Isdifferent = true;
                    }

                    if(hourMorning != restau.HourMorning)
                    {
                        restau.HourMorning = hourMorning;
                        Isdifferent = true;
                    }

                    if (hournight != restau.HourNight)
                    {
                        restau.HourNight = hournight;
                        Isdifferent = true;
                    }

                    if (Isdifferent == true)
                    {
                        DalRestaurant.Update(restau);
                       return RedirectToAction("Index", "Restaurant");
                    }
                }
            }

            if(Request.Cookies["userInfo"] != null && Request.Cookies["userInfo"]["Role"] == "Admins")
            {
                return RedirectToAction("Index", "Restaurant");
            }else
            {
                return RedirectToAction("Index", "Director");
            }
               
        }


        /// <summary>
        /// Get manager restaurant view
        /// </summary>
        /// <returns>View</returns>
        [HttpGet]
        public ActionResult ManagerRestaurant()
        {
            string CurrentCookie = HttpContext.User.Identity.Name;
            long IdDirector = long.Parse(CurrentCookie);
            long idRestaurant = DalDirector.GetOwnerById(IdDirector).CurrentRestaurantId;
            Restaurant = DalRestaurant.GetRestaurantById(idRestaurant);
            return View(Restaurant);
        }

        /// <summary>
        /// RGet all restaurant for anonymous user
        /// </summary>
        /// <param name="nameTown">string</param>
        /// <returns>RedirectToAction</returns>
        [HttpGet]
        [AllowAnonymous]
        public ActionResult AllRestaurant(string nameTown)
        {
            if(nameTown != null)
            {
               long idTown =  DalTown.GetIdTownByNameCity(nameTown);

                List<RestaurantEntity> RestautantsInTown = DalRestaurant.GetAllRestaurantByIdTown(idTown);
                ViewData["AllRestaurant"] = RestautantsInTown;
                return View("SeeAllRestaurantByTown");
            }
            return RedirectToAction("Index", "Home");
        }

        /// <summary>
        /// Make restaurant open for orders
        /// </summary>
        /// <returns>RedirectToAction</returns>
        [HttpGet]
        public ActionResult Open()
        {
            
            string CurrentCookie = HttpContext.User.Identity.Name;
            long IdDirector = long.Parse(CurrentCookie);
            long idRestaurant = DalDirector.GetOwnerById(IdDirector).CurrentRestaurantId;
            Restaurant = DalRestaurant.GetRestaurantById(idRestaurant);
            if (Restaurant.Open.Equals(false))
            {
                Restaurant.Open = true;
                DalRestaurant.Update(Restaurant);
            }
            return RedirectToAction("ManagerRestaurant", "Restaurant");

        }

        /// <summary>
        /// Make restaurant close for orders
        /// </summary>
        /// <returns>RedirectToAction</returns>
        [HttpGet]
        public ActionResult Close()
        {
            string CurrentCookie = HttpContext.User.Identity.Name;
            long IdDirector = long.Parse(CurrentCookie);
            long idRestaurant = DalDirector.GetOwnerById(IdDirector).CurrentRestaurantId;
            Restaurant = DalRestaurant.GetRestaurantById(idRestaurant);
            if (Restaurant.Open.Equals(true))
            {
                Restaurant.Open = false;
                DalRestaurant.Update(Restaurant);
            }
            return RedirectToAction("ManagerRestaurant", "Restaurant");
        }

        /// <summary>
        /// See restaurant for log user to order
        /// </summary>
        /// <param name="idRestaurant">long</param>
        /// <returns>View</returns>
        [HttpGet]
        public ActionResult SeeOneRestaurant(long idRestaurant)
        {
            string CurrentCookie = HttpContext.User.Identity.Name;
            long? IdUser = long.Parse(CurrentCookie);
            List<BurgerLikedUserEntity> BurgerLiked = new List<BurgerLikedUserEntity>();
            if (!IdUser.Equals(0))
            {
                using (DataBaseContext ctx = new DataBaseContext())
                {
                    BurgerLiked = ctx.BurgersLikedUsers.Where(r => r.UserId == IdUser).ToList();
                    ViewData["BurgerLiked"] = BurgerLiked;
                }

            }
            else
            {
                ViewData["BurgerLiked"] = null;
            }

            List<DrinkEntity> Drinks = DalDrink.GetAllDrinksInRestaurant(idRestaurant);
            ViewData["Drinks"] = Drinks;
            List<BurgerEntity> Burgers = DalBurger.GetAllBurgerInRestaurant(idRestaurant);
            ViewData["Burgers"] = Burgers;
            Restaurant = DalRestaurant.GetRestaurantById(idRestaurant);
            return View(Restaurant);
        }

        /// <summary>
        /// See all restaurant to anonymous user
        /// </summary>
        /// <returns>View</returns>
        [HttpGet]
        [AllowAnonymous]
        public ActionResult SeeAllRestaurant()
        {
            List<RestaurantEntity> AllRestaurant = new List<RestaurantEntity>();
            AllRestaurant = DalRestaurant.GetAllRestaurant();
            ViewData["AllRestaurant"] = AllRestaurant;
            List<TagEntity> Tags = DalTag.GetAllTag();
            ViewData["Tags"] = Tags;
            return View();
        }

        /// <summary>
        /// See restaurant by town to anonymous user
        /// </summary>
        /// <param name="idRestaurant">long</param>
        /// <returns>RedirectToAction</returns>
        [HttpGet]
        [AllowAnonymous]
        public ActionResult SeeRestaurantInTown(long idRestaurant)
        {
            List<DrinkEntity> Drinks = DalDrink.GetAllDrinksInRestaurant(idRestaurant);
            ViewData["Drinks"] = Drinks;
            List<BurgerEntity> Burgers = DalBurger.GetAllBurgerInRestaurant(idRestaurant);
            ViewData["Burgers"] = Burgers;
            Restaurant = DalRestaurant.GetRestaurantById(idRestaurant);
            return RedirectToAction("SeeRestaurant", "Restaurant", new { idRestaurant = idRestaurant });
        }

        /// <summary>
        /// See restaurant detail for anonymous user
        /// </summary>
        /// <param name="idRestaurant">long</param>
        /// <returns>View</returns>
        [HttpGet]
        [AllowAnonymous]
        public ActionResult SeeRestaurant(long idRestaurant)
        {
   
            List<DrinkEntity> Drinks = DalDrink.GetAllDrinksInRestaurant(idRestaurant);
            ViewData["Drinks"] = Drinks;
            List<BurgerEntity> Burgers = DalBurger.GetAllBurgerInRestaurant(idRestaurant);
            ViewData["Burgers"] = Burgers;
            Restaurant = DalRestaurant.GetRestaurantById(idRestaurant);
            return View("SeeOneRestaurant", Restaurant);
        }


    }

}