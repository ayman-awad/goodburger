﻿using GoodBurger.Models.DAL;
using GoodBurger.Models.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;


namespace GoodBurger.Controllers
{
    [Authorize]
    public class OrderController : Controller
    {
        private OrderDal DalOrder = new OrderDal();
        private DirectorDal DalDirector = new DirectorDal();
        private DirectorEntity Director = new DirectorEntity();
        /// <summary>
        /// Get idnex view
        /// </summary>
        /// <returns>View</returns>
        // GET: Order
        public ActionResult Index()
        {
            string CurrentCookie = HttpContext.User.Identity.Name;
            long idUser = long.Parse(CurrentCookie);
            List<OrderEntity> Orders = new List<OrderEntity>();
            Orders = DalOrder.GetAllOrders().OrderByDescending(r => r.DateOrder.Date).ToList();
            ViewData["Orders"] = Orders;
            return View();
        }

        /// <summary>
        ///Get all order
        /// </summary>
        /// <param name="allOrder">bool?</param>
        /// <param name="monthOrder">bool?</param>
        /// <param name="day">bool?</param>
        /// <returns>View</returns>
        public ActionResult SeeAllOrder(bool? allOrder,bool? monthOrder, bool? day)
        {
            string CurrentCookie = HttpContext.User.Identity.Name;
            long IdDirector = long.Parse(CurrentCookie);
            Director = DalDirector.GetOwnerById(IdDirector);
            long idRestaurant = Director.CurrentRestaurantId;
            int Day = DateTime.Now.Day;
            int  Month = DateTime.Now.Month;

            if (Director != null && allOrder != null) 
            {
                List<OrderEntity> Orders = DalOrder.GetAllOrderByIdRestaurant(idRestaurant).OrderByDescending(date => date.DateOrder).ToList();
                ViewData["Orders"] = Orders;
            }
            if (Director != null && monthOrder != null)
            {
                List<OrderEntity> OrderThisMonth = DalOrder.GetAllOrderByIdRestaurant(idRestaurant).Where(d => d.DateOrder.Month == Month).ToList();
                ViewData["Orders"] = OrderThisMonth;
            }


            if (Director != null && day != null)
            {
                List<OrderEntity> DayOrders = DalOrder.GetAllOrderByIdRestaurant(idRestaurant).Where(d => d.DateOrder.Day == Day).ToList();
                ViewData["Orders"] = DayOrders;
            }

            return View();
        }

        
    }
}