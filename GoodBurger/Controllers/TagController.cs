﻿using GoodBurger.Models.ContextDB;
using GoodBurger.Models.DAL;
using GoodBurger.Models.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace GoodBurger.Controllers
{
    [Authorize]
    public class TagController : Controller
    {
        private TagDal DalTag = new TagDal();
        private BurgerDal DalBurger = new BurgerDal();
        private RestaurantDal DalRestaurant = new RestaurantDal();

        private TagEntity Tag = new TagEntity();
        private RestaurantEntity Restaurant = new RestaurantEntity();
        private BurgerEntity MyBurger = new BurgerEntity();

        /// <summary>
        /// Get index view
        /// </summary>
        /// <returns>View</returns>
        public ActionResult Index()
        {
           List<TagEntity> AllTags = DalTag.GetAllTag();
           ViewData["AllTags"] = AllTags;

            return View();
        }

        /// <summary>
        /// Get form to add tag
        /// </summary>
        /// <returns>View</returns>
        [HttpGet]
        public ActionResult Add()
        {
            List<TagEntity> AllTags = DalTag.GetAllTag();
            ViewData["AllTags"] = AllTags;

            return View();
        }

        /// <summary>
        /// check and add tag in data base
        /// </summary>
        /// <param name="dataForm">FormCollection</param>
        /// <returns></returns>
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Add(FormCollection dataForm)
        {
            if(dataForm != null)
            {
                string Name = dataForm["Name"];
                TagEntity NewTag = new TagEntity(Name);
                DalTag.Add(NewTag);
                List<TagEntity> AllTags = DalTag.GetAllTag();
                ViewData["AllTags"] = AllTags;
            }

            return View();
        }

        /// <summary>
        /// Remove tag to data base
        /// </summary>
        /// <param name="Tagid">long</param>
        /// <returns>RedirectToAction</returns>
        [HttpGet]
        public ActionResult Remove(long Tagid)
        {
            if (!Tagid.Equals(0))
            {
                Tag = DalTag.GetTagById(Tagid);
                DalTag.Remove(Tag);
            }
            return RedirectToAction("Index", "Tag");
        }

        /// <summary>
        /// Remove tag in burger
        /// </summary>
        /// <param name="Tagid">long</param>
        /// <param name="BurgerId">long</param>
        /// <returns>RedirectToAction</returns>
        [HttpGet]
        public ActionResult RemoveTagBurger(long Tagid, long BurgerId)
        {
            if(!BurgerId.Equals(0) && !Tagid.Equals(0))
            {
                BurgerEntity MyBurger = new BurgerEntity();
                MyBurger = DalBurger.GetBurgerById(BurgerId);

                using(DataBaseContext ContextDB = new DataBaseContext())
                {
                    TagInBurgerEntity TagBurger = ContextDB.TagsInBurgers.Where(r => r.TagId == Tagid).Single();
                    ContextDB.TagsInBurgers.Remove(TagBurger);
                    ContextDB.SaveChanges();
                }
            }
            
            return RedirectToAction("FormUpdateBurger", "Burger", new { BurgerId = BurgerId } );
      
        }
     

    }
}