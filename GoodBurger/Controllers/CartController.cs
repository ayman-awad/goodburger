﻿using GoodBurger.Models.ContextDB;
using GoodBurger.Models.DAL;
using Pechkin;
using System;
using System.Collections.Generic;
using System.Data.Entity.Migrations;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace GoodBurger.Models.Entities
{
    [Authorize]
    public class CartController : Controller
    {
       private BurgerDal DalBurger = new BurgerDal();
       private DrinkDal DalDrink = new DrinkDal();
       private OrderDal DalOrder = new OrderDal();

       private static List<long?> ListDrinksId = new List<long?>();
       private static List<long?> ListBurgersId = new List<long?>();

       private static List<BurgerEntity> BurgerInOrder = new List<BurgerEntity>();
       private static List<DrinkEntity> DrinkInOrder = new List<DrinkEntity>();

        /// <summary>
        /// Get index view cart
        /// </summary>
        /// <param name="idRestaurant">long</param>
        /// <returns>View</returns>
        // GET: Cart
        public ActionResult Index(long? idRestaurant)
        {
            if (HttpContext.Session["Cart"] != null)
            {
                if(ListDrinksId.Count() != 0)
                {
                    List<DrinkEntity> Drinks = new List<DrinkEntity>();
                    foreach (long Drink in ListDrinksId)
                    {

                        Drinks.Add(DalDrink.GetDrinkById(Drink));
                        ViewData["Drinks"]=  Drinks;
                      
                    }
                }
                if (ListBurgersId.Count() != 0)
                {
                    List<BurgerEntity> Burgers = new List<BurgerEntity>();
                    foreach (long Burger in ListBurgersId)
                    {
                        Burgers.Add(DalBurger.GetBurgerById(Burger));
                        ViewData["Burgers"] = Burgers;
                    }
                }
                if(idRestaurant != null)
                {
                    if (DrinkInOrder.Count() != 0)
                    {
                        idRestaurant = DrinkInOrder[0].CurrentIdRestaurant;
                    }
                    else if (BurgerInOrder.Count() != 0)
                    {
                        idRestaurant = BurgerInOrder[0].CurrentIdRestaurant;
                    }
                }
              
            }
            return View();
        }

        /// <summary>
        /// Add product to cart
        /// </summary>
        /// <param name="idBurger">long</param>
        /// <param name="idDrink">long</param>
        /// <param name="idRestaurant">long</param>
        /// <returns></returns>
        public ActionResult Add(long? idBurger, long? idDrink,long idRestaurant)
        {
            if(HttpContext.Session["Cart"] == null)
            {
                if (!string.IsNullOrWhiteSpace(HttpContext.User.Identity.Name))
                {
                    HttpContext.Session["Cart"] = HttpContext.User.Identity.Name;
            
                }
            }

            if (HttpContext.Session["Cart"] != null) {

                if (!idBurger.Equals(0) && idBurger != null)
                {
                  
                    HttpContext.Session["burger"] = ListBurgersId;
                    ListBurgersId.Add(idBurger);
  

                }
                if (!idDrink.Equals(0) && idDrink != null)
                {
                    HttpContext.Session["drink"] = ListDrinksId;
                    ListDrinksId.Add(idDrink);
                }

                return RedirectToAction("Index","Cart", new { idRestaurant = idRestaurant });
            }

            return RedirectToAction("SeeOneRestaurant", "Restaurant", new { idRestaurant = idRestaurant });
        }

        /// <summary>
        /// Remove product from favoris
        /// </summary>
        /// <param name="Burgerid">long</param>
        /// <param name="Drinkid">long</param>
        /// <param name="idRestaurant">long</param>
        /// <returns></returns>
        public ActionResult Remove(long? Burgerid,long? Drinkid, long idRestaurant)
        {
            if(Burgerid != null)
            {
                ListBurgersId.Remove(Burgerid);
            }
            if (Drinkid != null)
            {
                ListDrinksId.Remove(Drinkid);
            }
            return RedirectToAction("Index", "Cart", new { idRestaurant = idRestaurant });
        }

        /// <summary>
        /// Check product add in cart 
        /// substract the product from the data base
        /// 
        /// </summary>
        /// <returns>RedirectToAction</returns>
        public ActionResult ValidCart()
        {
            float sumOrder = 0;
            GetDrinkAndGetRestaurant();
            string CurrentCookie = HttpContext.User.Identity.Name;
            long idUser = long.Parse(CurrentCookie);
            long idRestaurant = 0;

            BurgerEntity Burger = new BurgerEntity();
            DrinkEntity Drink = new DrinkEntity();

            if (DrinkInOrder.Count() != 0)
            {
                idRestaurant = DrinkInOrder[0].CurrentIdRestaurant;
            }
            else if(BurgerInOrder.Count() != 0)
            {
                idRestaurant = BurgerInOrder[0].CurrentIdRestaurant;
            }
   
            OrderEntity MyOrder = new OrderEntity(DateTime.Now, idUser, idRestaurant, sumOrder);
            long orderId = DalOrder.Add(MyOrder);
            
            if (DrinkInOrder.Any() && !orderId.Equals(0))
            {
                using(DataBaseContext ContextDB = new DataBaseContext())
                {
                    
                    foreach (DrinkEntity oneDrink in DrinkInOrder)
                    {
                        
                        long idDrink = oneDrink.DrinkId;

                        DrinksInOrdersEntity CheckIfOrderExiste = ContextDB.DrinksInOrders.Where(r => r.OrderId == orderId).SingleOrDefault();
                        if(CheckIfOrderExiste != null)
                        {
                            if(CheckIfOrderExiste.DrinkId == oneDrink.DrinkId)
                            {
                                CheckIfOrderExiste.Quantity = CheckIfOrderExiste.Quantity + 1;
                                ContextDB.DrinksInOrders.AddOrUpdate(CheckIfOrderExiste);
                                ContextDB.SaveChanges();
                                DrinkEntity MyDrink = DalDrink.GetDrinkById(CheckIfOrderExiste.DrinkId);
                                DalDrink.SubtractSale(MyDrink);
                            }
                            else
                            {
                                DrinksInOrdersEntity DrinksOrder = new DrinksInOrdersEntity(idDrink, CheckIfOrderExiste.OrderId, 1);
                                ContextDB.DrinksInOrders.Add(DrinksOrder);
                                ContextDB.SaveChanges();
                                DrinkEntity MyDrink = DalDrink.GetDrinkById(idDrink);
                                DalDrink.SubtractSale(MyDrink);
                            }
                        }else
                        {
                            DrinksInOrdersEntity DrinksOrder = new DrinksInOrdersEntity(idDrink, orderId, 1);
                            ContextDB.DrinksInOrders.Add(DrinksOrder);
                            ContextDB.SaveChanges();
                            DrinkEntity MyDrink = DalDrink.GetDrinkById(idDrink);
                            DalDrink.SubtractSale(MyDrink);
                        }

                        sumOrder += oneDrink.Price;
                        if (!sumOrder.Equals(0))
                        {
                            MyOrder.SumOrder = sumOrder;
                            DalOrder.Update(MyOrder);
                        }
                    }
                }
              
                ListDrinksId.Clear();
                DrinkInOrder.Clear();
            }

            if (BurgerInOrder.Any() && !orderId.Equals(0))
            {
                using (DataBaseContext ContextDB = new DataBaseContext())
                {
                    foreach (BurgerEntity oneBurger in BurgerInOrder)
                    {
                      
                        long idburger = oneBurger.BurgerId;

                        List<BurgerInOrderEntity> CheckIfOrderExiste = ContextDB.BurgersInOrders.Where(r => r.OrderId == orderId).ToList();
                        if(CheckIfOrderExiste.Count != 0)
                        { 
                            foreach (BurgerInOrderEntity BurgerOrder in CheckIfOrderExiste)
                            {
                           
                                    if (BurgerOrder.BurgerId == oneBurger.BurgerId)
                                    {
                                        BurgerOrder.Quantity = BurgerOrder.Quantity + 1;
                                        ContextDB.BurgersInOrders.AddOrUpdate(BurgerOrder);
                                        ContextDB.SaveChanges();
                                        BurgerEntity MyBurger = DalBurger.GetBurgerById(BurgerOrder.BurgerId);
                                        DalBurger.SubtractSale(MyBurger);
                                    }
                                    else
                                    {
                                        BurgerInOrderEntity DrinksOrder = new BurgerInOrderEntity(idburger, BurgerOrder.OrderId, 1);
                                        ContextDB.BurgersInOrders.Add(DrinksOrder);
                                        ContextDB.SaveChanges();
                                        BurgerEntity MyBurger = DalBurger.GetBurgerById(idburger);
                                        DalBurger.SubtractSale(MyBurger);
                                    }
                            }
                        }

             
                        if (CheckIfOrderExiste.Count == 0)
                        {
                            BurgerInOrderEntity BurgerOrder = new BurgerInOrderEntity(idburger, orderId, 1);
                            ContextDB.BurgersInOrders.Add(BurgerOrder);
                            ContextDB.SaveChanges();
                            BurgerEntity MyBurger = DalBurger.GetBurgerById(idburger);
                            DalBurger.SubtractSale(MyBurger);
                        }

                        sumOrder = oneBurger.Price + sumOrder;
                        if (!sumOrder.Equals(0))
                        {
                            MyOrder.SumOrder = sumOrder;
                            DalOrder.Update(MyOrder);
                        }
                    }

                }

                ListBurgersId.Clear();
                BurgerInOrder.Clear();
            }
            CreatePdf(orderId);
            return RedirectToAction("SeeOneRestaurant", "Restaurant", new { idRestaurant = idRestaurant });
        }

     /// <summary>
     /// Add drink and burger in list order
     /// </summary>
        [NonAction]
        private void GetDrinkAndGetRestaurant()
        {
            if (ListDrinksId.Count() != 0)
            {
                foreach(long drink in ListDrinksId)
                {
                    DrinkEntity Drink = DalDrink.GetDrinkById(drink);
                    DrinkInOrder.Add(Drink);
                }
            }

            if (ListBurgersId.Count() != 0)
            {
                foreach (long burger in ListBurgersId)
                {
                    BurgerEntity Burger = DalBurger.GetBurgerById(burger);
                    BurgerInOrder.Add(Burger);
                }
            }
        }

        /// <summary>
        /// Generat pdf from the order
        /// </summary>
        /// <param name="order">long</param>
        [NonAction]
        private void CreatePdf(long order)
        {
            List<DrinksInOrdersEntity> DrinkOrder = new List<DrinksInOrdersEntity>();
            List<BurgerInOrderEntity> BurgerOrder = new List<BurgerInOrderEntity>();

            List<DrinkEntity> Drinks = new List<DrinkEntity>();
            List<BurgerEntity> Burgers = new List<BurgerEntity>();

            OrderEntity Order = new OrderEntity();
            Order = DalOrder.GetOrdderById(order);
            using(DataBaseContext ctx = new DataBaseContext())
            {
                DrinkOrder = ctx.DrinksInOrders.Where(r => r.OrderId == order).ToList();
                BurgerOrder = ctx.BurgersInOrders.Where(r => r.OrderId == order).ToList();
            }


            foreach(DrinksInOrdersEntity Drink in DrinkOrder)
            {
                Drinks.Add (DalDrink.GetDrinkById(Drink.DrinkId));
            }

            foreach (BurgerInOrderEntity Burger in BurgerOrder)
            {
                Burgers.Add(DalBurger.GetBurgerById(Burger.BurgerId));
            }

            byte[] pdfContent = new SimplePechkin(new GlobalConfig()).Convert(
                "<html>" +
                "<body>" +
                "<h1>" + "Commande N° :" + " " + order + "</h1>" +
                "<h1>" + "Faite le :°" + " " + Order.DateOrder + "</h1>" +
                "<h1>" + "Total de la commande :" + " " + Order.SumOrder + "€" + "</h1>" +

                "<h2>" + " Détail de votre commande : </h2>" +
                "<div>" + "---------------------------------------------------------------------------------------------------------------------------------" + " </div>" +

                 "<div>" +
                 "Nom du Burger :"+" "+GetBurgerHtml(Burgers,BurgerOrder,order)
                 + "</div>" +

                "<div>" + "---------------------------------------------------------------------------------------------------------------------------------" + " </div>" +

                "<div>" +
                 "Nom de la Boisson :" + " " + GetDrinkHtml(Drinks, DrinkOrder, order)
                 + "</div>" +
                 "<div>" + "---------------------------------------------------------------------------------------------------------------------------------" + " </div>" +
                 ""+"<p>"+"Merci de votre commande et à bientôt ! "+"</p>"+
                 "</body>" +
                 "</html>");


            string directory = "C:\\Users\\l1\\Source\\Repos\\GoodBurger\\GoodBurger\\Content\\pdf\\";           
            // Name of the PDF
            string filename = order+".pdf";
            ByteArrayToFile(directory+ filename, pdfContent);
        }

        /// <summary>
        /// Get all burger in order and make a string with html
        /// </summary>
        /// <param name="Burger">List BurgerEntity</param>
        /// <param name="BurgersOrder">List BurgerInOrderEntity</param>
        /// <param name="order">long</param>
        /// <returns>String</returns>
        [NonAction]
        public String GetBurgerHtml(List<BurgerEntity> Burger , List<BurgerInOrderEntity> BurgersOrder , long order)
        {
            string BurgerHtml = "";
            string QuantityHtml = "";
            foreach (BurgerEntity burger in Burger)
            {
                foreach (BurgerInOrderEntity Burgers in BurgersOrder)
                {
                    if (Burgers.OrderId == order)
                    {
                        QuantityHtml = Burgers.Quantity.ToString();
                    }
                }
                BurgerHtml += "<p>" + burger.Name +" "+"X" + " "+QuantityHtml + " <br>" + "Prix du Burger"+" "+ burger.Price+"€"+ " "+"</p>";
            }

            return BurgerHtml;
        }
        /// <summary>
        /// Get all drink in order and make a string with html
        /// </summary>
        /// <param name="Drink"> list DrinkEntity</param>
        /// <param name="DrinksOrder"> list DrinksInOrdersEntity</param>
        /// <param name="order">long</param>
        /// <returns>String</returns>
        [NonAction]
        public String GetDrinkHtml(List<DrinkEntity> Drink, List<DrinksInOrdersEntity> DrinksOrder, long order)
        {
            string DrinkHtml = "";
            string QuantityHtml = "";
            foreach (DrinkEntity drink in Drink)
            {
                foreach(DrinksInOrdersEntity Drinks in DrinksOrder)
                {
                    if(Drinks.OrderId == order)
                    {
                        QuantityHtml = Drinks.Quantity.ToString();
                    }
                }
                DrinkHtml += "<p>" + drink.Name+" "+ "X"+ " " + QuantityHtml + "<br> "+"Prix de la boisson "+ drink.Price + "€" + " " + "</p>";
            }

            return DrinkHtml;
        }

        /// <summary>
        /// Save pdf 
        /// </summary>
        /// <param name="_FileName">string</param>
        /// <param name="_ByteArray">byte[]</param>
        /// <returns></returns>
        [NonAction]
        public bool ByteArrayToFile(string _FileName, byte[] _ByteArray)
        {
            try
            {
                // Open file for reading
                FileStream _FileStream = new FileStream(_FileName, FileMode.Create, FileAccess.Write);
                // Writes a block of bytes to this stream using data from  a byte array.
                _FileStream.Write(_ByteArray, 0, _ByteArray.Length);

                // Close file stream
                _FileStream.Close();

                return true;
            }
            catch (Exception _Exception)
            {
                Console.WriteLine("Exception caught in process while trying to save : {0}", _Exception.ToString());
            }

            return false;
        }
    }
}