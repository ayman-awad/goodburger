﻿using GoodBurger.Models.DAL;
using GoodBurger.Models.Entities;
using System;
using System.Collections.Generic;
using System.Web.Mvc;

namespace GoodBurger.Controllers
{
    [Authorize]
    public class TownController : Controller
    {
        private TownDal Dal = new TownDal();
        /// <summary>
        /// Get index view
        /// </summary>
        /// <returns>View</returns>
        // GET: Town
        [HttpGet]
        public ActionResult Index()
        {
            List<TownEntity> tonws = Dal.GetAllTown();
            ViewData["AllTowns"] = tonws;
            return View();
        }

        /// <summary>
        /// Check and add town
        /// </summary>
        /// <param name="dataForm">FormCollection</param>
        /// <returns>RedirectToAction</returns>
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Add(FormCollection dataForm)
        {

            if(dataForm != null)
            {
                string city = dataForm["City"];
                string country = dataForm["Country"];
                string CurrentCookie = HttpContext.User.Identity.Name;
                long idAdmin = long.Parse(CurrentCookie);
                TownEntity town = new TownEntity(city, country, idAdmin);
                long id = Dal.Add(town);

                if (!id.Equals(0))
                {
                    return RedirectToAction("Index", "Town");
                }
            }
          
            return RedirectToAction("Index", "Admin");
        }

        /// <summary>
        /// get form add town
        /// </summary>
        /// <returns>View</returns>
        [HttpGet]
        public ActionResult FormAdd()
        {
            return View();
        }

        /// <summary>
        /// Get form to update town
        /// </summary>
        /// <param name="TownId">long</param>
        /// <returns>RedirectToAction</returns>
        [HttpGet]
        public ActionResult FormUpdate(long TownId)
        {
            if (!TownId.Equals(0))
            {
                TownEntity town = Dal.GetAllInformationTownById(TownId);
                return View(town);
            }
            return RedirectToAction("Index", "Admin");
        }

        /// <summary>
        /// Check and update town in data base
        /// </summary>
        /// <param name="idTown">long</param>
        /// <param name="dataForm">FormCollection</param>
        /// <returns>RedirectToAction</returns>
        [HttpPost]
        public ActionResult Update(long idTown, FormCollection dataForm)
        {
            TownEntity town = Dal.GetAllInformationTownById(idTown);

            if (dataForm != null)
            {

                Boolean Isdifferent = false;
                string city = dataForm["City"];
                string country = dataForm["Country"];
               

                if (town != null)
                {

                    if (city != town.City)
                    {
                        town.City = city;
                        Isdifferent = true;
                    }
                    if (country != town.Country)
                    {
                        town.Country = country;
                        Isdifferent = true;
                    }

                    if (Isdifferent == true)
                    {
                        Dal.UpdateTown(town);
                        return RedirectToAction("Index", "Town");
                    }

                }
               
            }
            return RedirectToAction("Index", "Admin");
        }

        /// <summary>
        /// Remove town from data base
        /// </summary>
        /// <param name="TownId">long</param>
        /// <returns>RedirectToAction</returns>
        [HttpGet]
        public ActionResult Remove(long TownId)
        {
            if (!TownId.Equals(0))
            {
                TownEntity town = Dal.GetAllInformationTownById(TownId);
                Dal.RemoveTown(town);
                return RedirectToAction("Index", "Town");
            }
            return RedirectToAction("Index", "Admin");
        }

    }

}