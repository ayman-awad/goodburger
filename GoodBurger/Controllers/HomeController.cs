﻿
using System.Web.Mvc;

namespace GoodBurger.Controllers
{
    [AllowAnonymous]
    public class HomeController : Controller
    {

        public ActionResult Index()
        {
            return View();
        }

      
        public ActionResult Contact()
        {
            ViewBag.Message = "Nos coordonnés";

            return View();
        }
    }
}