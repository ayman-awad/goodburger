﻿using GoodBurger.Models.DAL;
using GoodBurger.Models.Entities;
using System;
using System.Collections.Generic;
using System.Web.Mvc;

namespace GoodBurger.Controllers
{
    [Authorize]
    public class DrinkController : Controller
    {
        private DrinkEntity Drink = null;
        private DrinkDal Dal = new DrinkDal();
        private RestaurantDal DalRestaurant = new RestaurantDal();
        private DirectorEntity Director = null;
        private DirectorDal DalDirector = new DirectorDal();

        /// <summary>
        /// Get index view
        /// </summary>
        /// <returns>View</returns>
        // GET: Drink
        [HttpGet]
        public ActionResult Index()
        {
            string CurrentCookie = HttpContext.User.Identity.Name;
            long idUser = long.Parse(CurrentCookie);

            Director = DalDirector.GetOwnerById(idUser);
            List<DrinkEntity> AllDrinks = Dal.GetAllDrinksInRestaurant(Director.CurrentRestaurantId);
            ViewData["AllDrinks"] = AllDrinks;
            return View();
        }

        /// <summary>
        /// Get drom add drink
        /// </summary>
        /// <returns>View</returns>
        [HttpGet]
        // GET: Ingredient
        public ActionResult FormAddDrink()
        {
            return View();
        }

        /// <summary>
        /// Form update drink
        /// </summary>
        /// <param name="DrinkId">long</param>
        /// <returns>View</returns>
        [HttpGet]
        // GET: Ingredient
        public ActionResult FormUpdateDrink(long DrinkId)
        {
            Drink = Dal.GetDrinkById(DrinkId);
            return View(Drink);
        }

        /// <summary>
        /// Check and update data drink
        /// </summary>
        /// <param name="dataForm">FormCollection</param>
        /// <param name="DrinkId">long</param>
        /// <returns>RedirectToAction</returns>
        [HttpPost]
        [ValidateAntiForgeryToken]
        //POST: Ingredient
        public ActionResult Update(FormCollection dataForm, long DrinkId)
        {

            Drink = Dal.GetDrinkById(DrinkId);

            if (dataForm != null)
            {

                Boolean Isdifferent = false;
                string Name = dataForm["Name"];
                int QuantityCentiliter = int.Parse(dataForm["QuantityCentiliter"]);
                string ImageUrl = dataForm["ImageUrl"];
                int Stock = int.Parse(dataForm["Stock"]);

                if (Drink != null)
                {

                    if (Name != Drink.Name)
                    {
                        Drink.Name = Name;
                        Isdifferent = true;
                    }

                    if (QuantityCentiliter != Drink.QuantityCentiliter)
                    {
                        Drink.QuantityCentiliter = QuantityCentiliter;
                        Isdifferent = true;
                    }
                    
                    if (ImageUrl != Drink.ImageUrl)
                    {
                        Drink.ImageUrl = ImageUrl;
                        Isdifferent = true;
                    }

                    if (Isdifferent == true)
                    {
                        Dal.Update(Drink);

                    }
                }
            }
            return RedirectToAction("Index", "Drink");
        }
        /// <summary>
        /// check and add drink in data base
        /// </summary>
        /// <param name="dataForm">FormCollection</param>
        /// <returns>RedirectToAction</returns>
        [HttpPost]
        [ValidateAntiForgeryToken]
        //POST: Ingredient
        public ActionResult Add(FormCollection dataForm)
        {
            try
            {
                string Name = dataForm["Name"];
                int QuantityMilliter = int.Parse(dataForm["QuantityCentiliter"]);
                string ImageUrl = dataForm["ImageUrl"];
                int Stock = int.Parse(dataForm["Stock"]);
                float Price = float.Parse(dataForm["Price"]);

                string CurrentCookie = HttpContext.User.Identity.Name;
                long idUser = long.Parse(CurrentCookie);
                Director = DalDirector.GetOwnerById(idUser);
                

                Drink = new DrinkEntity(Name, QuantityMilliter, ImageUrl, Stock, Director.CurrentRestaurantId, Price);
                if (Drink != null)
                {
                    long id = Dal.Add(Drink);     
                }

           }
           catch (Exception e)
            {
                ModelState.AddModelError("Error in register action", e.Message);
            }

            return RedirectToAction("Index", "Drink");
        }

        /// <summary>
        /// Remove drink from data base
        /// </summary>
        /// <param name="DrinkId">long</param>
        /// <returns>RedirectToAction</returns>
        public ActionResult Remove(long DrinkId)
        {
            if (!DrinkId.Equals(0))
            {
                Drink = Dal.GetDrinkById(DrinkId);
                Dal.Remove(Drink);

            }
            return RedirectToAction("Index", "Drink");
        }
    }
}
