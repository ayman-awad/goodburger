﻿using GoodBurger.Models.ContextDB;
using GoodBurger.Models.DAL;
using GoodBurger.Models.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;

namespace GoodBurger.Controllers
{
    [Authorize]
    public class BurgerController : Controller
    {
        private IngredientDal DalIngredient = new IngredientDal();
        private BurgerDal DalBurger = new BurgerDal();
        private DirectorDal DalDirector = new DirectorDal();
        private UserDal DalUser = new UserDal();

        /// <summary>
        /// Get index view Burger
        /// </summary>
        /// <returns>View</returns>
        // GET: Burger
        [HttpGet]
        public ActionResult Index()
        {
            string CurrentCookie = HttpContext.User.Identity.Name;
            long IdDirector = long.Parse(CurrentCookie);
            DirectorEntity Director = DalDirector.GetOwnerById(IdDirector);
            
            List<BurgerEntity> AllBurgers = DalBurger.GetAllBurgerInRestaurant(Director.CurrentRestaurantId);
            ViewData["AllBurgers"] = AllBurgers;
            return View();
        }

        /// <summary>
        /// Get form to add Burger
        /// </summary>
        /// <returns>View</returns>
        [HttpGet]
        public ActionResult FormAddBurger()
        {
            string CurrentCookie = HttpContext.User.Identity.Name;
            long IdDirector = long.Parse(CurrentCookie);
            DirectorEntity Director = DalDirector.GetOwnerById(IdDirector);
            List<IngredientEntity> AllIngredients = DalIngredient.GetAllIngredientByIdRestaurant(Director.CurrentRestaurantId);
            ViewData["AllIngredients"] = AllIngredients;
            using (DataBaseContext ContextDB = new DataBaseContext())
            {
                List<TagEntity> AllTags = new List<TagEntity>();
                AllTags = ContextDB.Tags.Select(row => row).ToList();
                ViewData["AllTags"] = AllTags;
            }
                return View();
        }

        /// <summary>
        /// Check burger inforamtion and add then in dat abase
        /// </summary>
        /// <param name="dataForm">FormCollection data to update</param>
        /// <returns>View</returns>
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult AddBurger(FormCollection dataForm)
        {
           try
            {
                if(dataForm != null)
                {
                    string Name = dataForm["Name"];
                    int Stock = int.Parse(dataForm["Stock"]);
                    float Price = float.Parse(dataForm["Price"]);
                    string ImageUrl = dataForm["ImageUrl"];
                    string Ingredients = dataForm["Ingredients[]"];
                    List<string> IngredientInBurger = Ingredients.Split(',').ToList();
                    string Tag = dataForm["Tags[]"];
                    List<string> TagsInBurger = Tag.Split(',').ToList();

                    string CurrentCookie = HttpContext.User.Identity.Name;
                    long IdDirector = long.Parse(CurrentCookie);
                    DirectorEntity Director = DalDirector.GetOwnerById(IdDirector);
                    BurgerEntity NewBurger = new BurgerEntity(Name,Stock,Price,ImageUrl, Director.CurrentRestaurantId);

                    if(NewBurger != null)
                    {
                        long idBurger = DalBurger.Add(NewBurger);
                        foreach (String Ingredient in IngredientInBurger)
                        {
                            long idIngredient = long.Parse(Ingredient);
                            using(DataBaseContext ContextDB = new DataBaseContext())
                            {
                                IngredientInBurgerEntity ingredientBurger = new IngredientInBurgerEntity(idIngredient, idBurger);
                                ContextDB.IngredientsInBurgers.Add(ingredientBurger);
                                ContextDB.SaveChanges();
                            }
                        }
                        foreach (String MyTag in TagsInBurger)
                        {
                            long IdTag = long.Parse(MyTag);
                            using (DataBaseContext ContextDB = new DataBaseContext())
                            {
                                TagInBurgerEntity TagBurger = new TagInBurgerEntity(IdTag, idBurger);
                                ContextDB.TagsInBurgers.Add(TagBurger);
                                ContextDB.SaveChanges();
                            }
                        }

                        List<BurgerEntity> AllBurgers = DalBurger.GetAllBurgerInRestaurant(Director.CurrentRestaurantId);
                       ViewData["AllBurgers"] = AllBurgers;
                       return RedirectToAction("Index", "Burger");
                    }
                }

            }
            catch (Exception e)
            {
                ModelState.AddModelError("Error in burger registration", e.Message);
            }
            return RedirectToAction("Index", "Burger");
        }

        /// <summary>
        /// Get form update burger
        /// </summary>
        /// <param name="BurgerId">id burger to update</param>
        /// <returns></returns>
        [HttpGet]
        public ActionResult FormUpdateBurger(long BurgerId)
        {
            BurgerEntity MyBurger = new BurgerEntity();
            MyBurger = DalBurger.GetBurgerById(BurgerId);
            return View(MyBurger);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="dataForm">FormCollection data to update</param>
        /// <param name="BurgerId">id burger to update</param>
        /// <returns></returns>
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Update(FormCollection dataForm, long BurgerId)
        {
            BurgerEntity Burger = DalBurger.GetBurgerById(BurgerId);

            if (dataForm != null)
            {
                Boolean Isdifferent = false;
                string Name = dataForm["Name"];
                int Stock = int.Parse(dataForm["Stock"]);
                int Price = int.Parse(dataForm["Price"]);
                string ImageUrl = dataForm["ImageUrl"];

                if (Burger != null)
                {

                    if (Name != Burger.Name)
                    {
                        Burger.Name = Name;
                        Isdifferent = true;
                    }
                    if (Stock != Burger.Stock)
                    {
                        Burger.Stock = Stock;
                        Isdifferent = true;
                    }
                    if (Price != Burger.Price)
                    {
                        Burger.Price = Price;
                        Isdifferent = true;
                    }
                    if (Price != Burger.Price)
                    {
                        Burger.Price = Price;
                        Isdifferent = true;
                    }
                    if (ImageUrl != Burger.ImageUrl)
                    {
                        Burger.ImageUrl = ImageUrl;
                        Isdifferent = true;
                    }
                    if (Isdifferent == true)
                    {
                        DalBurger.Update(Burger);
                        return RedirectToAction("Index", "Burger");
                    }
                }
            }
            return RedirectToAction("Index", "Burger");
        }


        /// <summary>
        /// Remove burger from data base
        /// </summary>
        /// <param name="BurgerId">id burger to Remove</param>
        /// <returns>RedirectToAction</returns>
        [HttpGet]
        public ActionResult Remove(long BurgerId)
        {
            if (!BurgerId.Equals(0))
            {
                BurgerEntity Burger = DalBurger.GetBurgerById(BurgerId);
                DalBurger.Remove(Burger);
                return RedirectToAction("Index", "Burger");
            }
            return RedirectToAction("ManagerRestaurant", "Restaurant");
        }

        /// <summary>
        /// Add burger to favori
        /// </summary>
        /// <param name="idBurger">id burger to add in favori</param>
        /// <param name="idRestaurant"> id restaurant </param>
        /// <returns>RedirectToAction</returns>
        [HttpGet]
        public ActionResult FavorisAdd(long idBurger, long idRestaurant)
        {
            string CurrentCookie = HttpContext.User.Identity.Name;
            long IdUser = long.Parse(CurrentCookie);
            UserEntity User = DalUser.GetUserById(IdUser);
            if(User != null)
            {
                using(DataBaseContext ContextDb = new DataBaseContext())
                {
                    BurgerLikedUserEntity BurgerLike = new BurgerLikedUserEntity(idBurger,IdUser);
                    ContextDb.BurgersLikedUsers.Add(BurgerLike);
                    ContextDb.SaveChanges();
                }
            }
            return RedirectToAction("See", "Restaurant" , new { idRestaurant = idRestaurant });
        }

        /// <summary>
        /// Remove urger to favori
        /// </summary>
        /// <param name="idBurger">id burger to add in favori</param>
        /// <param name="idRestaurant"> id restaurant </param>
        /// <returns>RedirectToAction</returns>
        [HttpGet]
        public ActionResult FavorisRemove(long idBurger, long idRestaurant)
        {
            string CurrentCookie = HttpContext.User.Identity.Name;
            long IdUser = long.Parse(CurrentCookie);
            UserEntity User = DalUser.GetUserById(IdUser);
            if (User != null)
            {
                using (DataBaseContext ContextDb = new DataBaseContext())
                {
                    BurgerLikedUserEntity BurgerLike = ContextDb.BurgersLikedUsers.Where(r => r.BurgerId == idBurger).SingleOrDefault();
                    ContextDb.BurgersLikedUsers.Remove(BurgerLike);
                    ContextDb.SaveChanges();
                }
            }
            return RedirectToAction("See", "Restaurant", new { idRestaurant = idRestaurant });
        }
    }
}