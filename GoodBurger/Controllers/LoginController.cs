﻿using GoodBurger.Classes;
using GoodBurger.Models.DAL;
using GoodBurger.Models.Entities;
using System;
using System.Web.Mvc;
using System.Web.Security;

namespace GoodBurger.Controllers
{
    [AllowAnonymous]
    public class LoginController : Controller
    {

        private UserDal dal = new UserDal();

        /// <summary>
        /// Get index view
        /// </summary>
        /// <returns>View</returns>
        // GET: Login
        public ActionResult Index()
        {
            return View();
        }

        /// <summary>
        /// check and log user
        /// </summary>
        /// <param name="dataForm">FormCollection</param>
        /// <returns>View</returns>
        //POST : Login
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Login(FormCollection dataForm)
        {
           
            try
            {

                UserEntity user = null;
                string email = dataForm["Email"];
                string password = PasswordHandler.MD5Hash(dataForm["Password"]);

                
                if(dal.PasswordIsValide(password))
                {
                    user = dal.CheckIfUserExist(email, password);
                    if (user.AccountBlock == false) {

                        HttpContext.Session["Role"] = "Users";
                        FormsAuthentication.SetAuthCookie(user.UserID.ToString(), false);
                        return RedirectToAction("Index", "User");
                    }
                    else{
                         return RedirectToAction("LoginError", "Error");
                    }
                }

            }  catch (Exception e){
                 ModelState.AddModelError("", e.Message);
            }

            return Redirect("/");
        }
    }
}