﻿using GoodBurger.Classes;
using GoodBurger.Models.DAL;
using GoodBurger.Models.Entities;
using System;
using System.Collections.Generic;
using System.Web.Mvc;
using System.Web.Security;

namespace GoodBurger.Controllers
{ 
    [Authorize]
    public class DirectorController : Controller
    {
        private DirectorDal DalDirector = new DirectorDal();
        private DirectorEntity Director = new DirectorEntity();
        private RestaurantDal DalRestaurant = new RestaurantDal();
        private OrderDal DalOrder = new OrderDal();
        private DrinkDal DalDrink = new DrinkDal();
        private BurgerDal DalBurger = new BurgerDal();

        // GET: OwnerRestaurant
        /// <summary>
        /// Get view index Director
        /// </summary>
        /// <returns>View</returns>
        [HttpGet]
        public ActionResult Index()
        {
            List<DirectorEntity> AllOwner = DalDirector.GetAllDirectors();
            ViewData["AllOwner"] = AllOwner;
            return View();
        }

        /// <summary>
        /// Get home Director view
        /// </summary>
        /// <returns>View</returns>
        public ActionResult HomeDirector()
        {
            string CurrentCookie = HttpContext.User.Identity.Name;
            long idUser = long.Parse(CurrentCookie);
            Director  = DalDirector.GetOwnerById(idUser);
            List<OrderEntity> Orders = DalOrder.GetAllOrderByIdRestaurant(Director.CurrentRestaurantId);
            ViewData["Orders"] = Orders;
            List<DrinkEntity> Drinks = DalDrink.GetAllDrinksInRestaurant(Director.CurrentRestaurantId);
            ViewData["Drinks"] = Drinks;
            List<BurgerEntity> Burgers = DalBurger.GetAllBurgerInRestaurant(Director.CurrentRestaurantId);
            ViewData["Burgers"] = Burgers;

            List<DrinkEntity> StockWarningDrink = new List<DrinkEntity>();
            List<BurgerEntity> StockWarningBurger = new List<BurgerEntity>();
            foreach (DrinkEntity Drink in Drinks)
            {
                if (Drink.Stock <= 20)
                {
                    
                    StockWarningDrink.Add(Drink);
                    ViewData["StockWarningDrink"] = StockWarningDrink;
                }
               
            }

            foreach (BurgerEntity Burger in Burgers)
            {
                if (Burger.Stock <= 20)
                {

                    StockWarningBurger.Add(Burger);
                    ViewData["StockWarningBurger"] = StockWarningBurger;
                }
                
            }

            return View();
        }

        /// <summary>
        /// Check and log director
        /// </summary>
        /// <param name="dataForm">FormCollection</param>
        /// <returns>RedirectToAction</returns>
        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        public ActionResult Login(FormCollection dataForm)
        {
            try
            {
                DirectorDal dal = new DirectorDal();
                string email = dataForm["Email"];
                string password = PasswordHandler.MD5Hash(dataForm["Password"]);
                Director = dal.CheckIfOwnerExist(email, password);
          
                if (Director != null)
                {
          
                    HttpContext.Session["Role"] = "Directors";
                    FormsAuthentication.SetAuthCookie(Director.UserID.ToString(), false);
                    return RedirectToAction("HomeDirector", "Director");
                }

               

            }
            catch (Exception e)
            {
               ModelState.AddModelError("Error", e.Message);
            }
            return RedirectToAction("Index", "Home");
        }

        /// <summary>
        /// Get form director
        /// </summary>
        /// <returns>View</returns>
        public ActionResult FormAddDirector()
        {

            List<DirectorEntity> Directors = DalDirector.GetAllDirectors();
            List<RestaurantEntity> AllRestaurant = DalRestaurant.GetAllRestaurant();
            
            ViewData["AllRestaurant"] = AllRestaurant;
            return View();
        }

        /// <summary>
        /// Get form login
        /// </summary>
        /// <returns>View</returns>
        [AllowAnonymous]
        public ActionResult FormLogin()
        {
            return View();
        }

        /// <summary>
        /// Check and add director in data base
        /// </summary>
        /// <param name="dataForm">FormCollection</param>
        /// <returns>RedirectToAction</returns>
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Add(FormCollection dataForm)
        {
            try
            {
                string firstName = dataForm["FirstName"];
                string lasttName = dataForm["LastName"];
                int phone = int.Parse(dataForm["Phone"]);
                string email = dataForm["Email"];
                string password = PasswordHandler.MD5Hash(dataForm["Password"]);
                string address = dataForm["Address"];
                int postalCode = int.Parse(dataForm["PostalCode"]);
                string city = dataForm["City"];
                long IdRestaurant = long.Parse(dataForm["IdRestaurant"]);

                string CurrentCookie = HttpContext.User.Identity.Name;
                long idAdmin = long.Parse(CurrentCookie);

                DirectorEntity owner = new DirectorEntity(firstName, lasttName, phone, email, password, address, postalCode, city, idAdmin, IdRestaurant);

                if (owner != null)
                {
                    long idOwer = DalDirector.Add(owner);
                    if (!idOwer.Equals(0))
                    {
                        List<DirectorEntity> AllOwner = DalDirector.GetAllDirectors();
                        ViewData["AllOwner"] = AllOwner;
                        return RedirectToAction("Index", "Director");
                    }

                }
            }

            catch (Exception e)
            {
                ModelState.AddModelError("Error in register action", e.Message);
            }

            return RedirectToAction("Index", "Director");
        }

        /// <summary>
        /// Get from update udirector
        /// </summary>
        /// <param name="DirectorId">long</param>
        /// <returns>View</returns>
        public ActionResult FormUpdateDirector(long DirectorId)
        {
            if (!DirectorId.Equals(0))
            {
                DirectorEntity Owner = DalDirector.GetOwnerById(DirectorId);
                return View(Owner);
            }
           
            return View();
        }

        /// <summary>
        /// Remove director from data base
        /// </summary>
        /// <param name="DirectorId">long</param>
        /// <returns>RedirectToAction</returns>
        [HttpGet]
        public ActionResult Remove(long DirectorId)
        {
            if (!DirectorId.Equals(0))
            {
                DirectorEntity Owner = DalDirector.GetOwnerById(DirectorId);
                DalDirector.Remove(Owner);
                
            }
            return RedirectToAction("Index", "Director");
        }

        /// <summary>
        /// check and update data director
        /// </summary>
        /// <param name="dataForm">FormCollection</param>
        /// <param name="DirectorId">long</param>
        /// <returns>RedirectToAction</returns>
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Update(FormCollection dataForm, long DirectorId)
        {
            DirectorEntity Director = DalDirector.GetOwnerById(DirectorId);


            if (dataForm != null)
            {

                Boolean Isdifferent = false;
                string firstName = dataForm["FirstName"];
                string lasttName = dataForm["LastName"];
                int phone = int.Parse(dataForm["Phone"]);
                string email = dataForm["Email"];
                string password = dataForm["Password"];
                string address = dataForm["Address"];
                int postalCode = int.Parse(dataForm["PostalCode"]);
                string city = dataForm["City"];

                if (Director != null)
                {

                    if (firstName != Director.FirstName)
                    {
                        Director.FirstName = firstName;
                        Isdifferent = true;
                    }

                    if (lasttName != Director.LastName)
                    {
                        Director.LastName = lasttName;
                        Isdifferent = true;
                    }
                    if (phone != Director.Phone)
                    {
                        Director.Phone = phone;
                        Isdifferent = true;
                    }
                    if (email != Director.Email)
                    {
                        Director.Email = email;
                        Isdifferent = true;
                    }

                    if (password != Director.Password)
                    {
                        Director.Password = password;
                        Isdifferent = true;
                    }

                    if (address != Director.Address)
                    {
                        Director.Address = address;
                        Isdifferent = true;
                    }
                    if (city != Director.City)
                    {
                        Director.City = city;
                        Isdifferent = true;
                    }
                    if (postalCode != Director.PostalCode)
                    {
                        Director.PostalCode = postalCode;
                        Isdifferent = true;
                    }

                    if (Isdifferent == true)
                    {
                        DalDirector.Update(Director);
                    }

                }

            }
            return RedirectToAction("Index", "Director");
        }




    }
}