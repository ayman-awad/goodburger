﻿using GoodBurger.Classes;
using GoodBurger.Models.DAL;
using GoodBurger.Models.Entities;
using System;
using System.Collections.Generic;
using System.Web.Mvc;
using System.Web.Security;

namespace GoodBurger.Controllers
{
    [Authorize]
    public class AdminController : Controller
    {
        private AdministratorEntity admin = new AdministratorEntity();
        private AdminDal dal = new AdminDal();
        private UserDal DalUser = new UserDal();
        private OrderDal DalOrder = new OrderDal();
        // GET: Admin
        [HttpGet]
        [AllowAnonymous]
        public ActionResult Index()
        {
            return View();
        }
        /// <summary>
        /// Get form information admin and login if is exist 
        /// </summary>
        /// <param name="dataForm">FormCollection table with form data</param>
        /// <returns>View</returns>
        // Post: Login 
        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        public ActionResult Login(FormCollection dataForm)
        {
            try
            {
                AdminDal dal = new AdminDal();
                string email = dataForm["EmailAdmin"];
                string password = PasswordHandler.MD5Hash(dataForm["PasswordAdmin"]);
                admin = dal.CheckIfUserExist(email, password);

                if (admin != null)
                {
                    FormsAuthentication.SetAuthCookie(admin.UserID.ToString(), false);
                    HttpContext.Session["Role"] = "Admins";
                    HttpContext.Session["RoleAdmin"] = admin.Role;
                    return RedirectToAction("HomeAdmin", "Admin");
                }

            }
            catch (Exception e)
            {
                ModelState.AddModelError("Error", e.Message);
            }
            return RedirectToAction("ManagerAdmin", "Home");
        }

        // GET: HomeAdmin
        /// <summary>
        /// Get home admin view if admin is login
        /// </summary>
        /// <returns>View</returns>
        [HttpGet]
        public ActionResult HomeAdmin()
        {

            if (HttpContext.User.Identity.IsAuthenticated)
            {
                string CurrentCookie = HttpContext.User.Identity.Name;
                long IdAdmin = long.Parse(CurrentCookie);
                admin = dal.GetAdminById(IdAdmin);
              
                List<UserEntity> Users = DalUser.GetAllUsers();
                ViewData["Users"] = Users;

                List<OrderEntity> Orders = DalOrder.GetAllOrders();
                ViewData["Orders"] = Orders;

                return View(admin);
            }

            return RedirectToAction("Index", "Admin");
        }

        /// <summary>
        /// Get Manager admin view if admin is login
        /// </summary>
        /// <returns>View</returns>
        // GET: ManagerAdmin
        [HttpGet]
        public ActionResult ManagerAdmin()
        {

            if (HttpContext.User.Identity.IsAuthenticated)
            {
                string CurrentCookie = HttpContext.User.Identity.Name;
                long IdAdmin = long.Parse(CurrentCookie);
                admin = dal.GetAdminById(IdAdmin);
                List<AdministratorEntity> AllAdminUser = dal.GetAllAdminUsers(IdAdmin);
                ViewData["AllAdminUser"] = AllAdminUser;
                return View(admin);
            }

            return RedirectToAction("Index", "Admin");
        }

        /// <summary>
        /// Get the form to add user admin
        /// </summary>
        /// <returns>View</returns>
        //GET :  FormUserAdmin
        [HttpGet]
        public ActionResult FormUserAdmin()
        {
            string CurrentCookie = HttpContext.User.Identity.Name;
            long IdAdmin = long.Parse(CurrentCookie);
            admin = dal.GetAdminById(IdAdmin);
            if(admin.Role != "full")
            {

            }
            return View();
        }
        /// <summary>
        /// Check data form post and add admin in the data base
        /// </summary>
        /// <param name="dataForm">FormCollection table with form data</param>
        /// <returns>RedirectToAction</returns>
        //POST : AddAdmins
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult AddAdmin(FormCollection dataForm)
        {
            if (dataForm != null)
            {
                string firstName = dataForm["FirstName"];
                string lasttName = dataForm["LastName"];
                int phone = int.Parse(dataForm["Phone"]);
                string email = dataForm["EmailAdmin"];
                string password = PasswordHandler.MD5Hash(dataForm["PasswordAdmin"]);
                string role = dataForm["Role"];

                AdministratorEntity adminUser = new AdministratorEntity(email, password, role, firstName, lasttName, phone);
                long id = dal.Add(adminUser);

                if (!id.Equals(0))
                {
                    return RedirectToAction("HomeAdmin", "Admin");
                }
            }
            return RedirectToAction("Index", "Admin");
        }

        /// <summary>
        /// Remove admin from data bas
        /// </summary>
        /// <param name="idadmin">id admin</param>
        /// <returns>RedirectToAction</returns>
        //GET : Remove
        [HttpGet]
        public ActionResult Remove(long idadmin)
        {
            if (!idadmin.Equals(0))
            {
                AdministratorEntity admin = dal.GetAdminById(idadmin);
                dal.Remove(admin);
            }

            return RedirectToAction("HomeAdmin", "Admin");
        }

        /// <summary>
        /// Get form to update user
        /// </summary>
        /// <param name="idadmin">id admin to update</param>
        /// <returns>View</returns>
        public ActionResult FormUpdate(long idadmin)
        {
            if (!idadmin.Equals(0))
            {
                AdministratorEntity admin = dal.GetAdminById(idadmin);
                return View(admin);
            }
            return RedirectToAction("Index", "Admin");
        }

        /// <summary>
        /// Check and update admin in data base
        /// </summary>
        /// <param name="idAdmin">id admin to upadte </param>
        /// <param name="dataForm">FormCollection data post</param>
        /// <returns></returns>
        //GET : Update
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Update(long idAdmin, FormCollection dataForm)
        {
            if (!idAdmin.Equals(0))
            {
                string firstName = dataForm["FirstName"];
                string lasttName = dataForm["LastName"];
                int phone = int.Parse(dataForm["Phone"]);
                string email = dataForm["EmailAdmin"];
                string password = PasswordHandler.MD5Hash(dataForm["PasswordAdmin"]);
                string role = dataForm["Role"];

                AdministratorEntity admin = dal.GetAdminById(idAdmin);
                Boolean Isdifferent = false;
                if (admin != null)
                {

                    if (firstName != admin.FirstName)
                    {
                        admin.FirstName = firstName;
                        Isdifferent = true;
                    }
                    if (lasttName != admin.LastName)
                    {
                        admin.LastName = lasttName;
                        Isdifferent = true;
                    }
                    if (email != admin.Email)
                    {
                        admin.Email = email;
                        Isdifferent = true;
                    }
                    if (password != admin.Password)
                    {
                        admin.Password = password;
                        Isdifferent = true;
                    }
                    if (phone != admin.Phone)
                    {
                        admin.Phone = phone;
                        Isdifferent = true;
                    }

                    if (role != admin.Role)
                    {
                        admin.Role = role;
                        Isdifferent = true;
                    }

                    if (Isdifferent == true)
                    {
                        dal.Update(admin);
                        return RedirectToAction("HomeAdmin", "Admin");
                    }

                }

            }
            return RedirectToAction("Index", "Admin");
        }
    }
}