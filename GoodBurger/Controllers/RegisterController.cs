﻿using GoodBurger.Classes;
using GoodBurger.Models.DAL;
using GoodBurger.Models.Entities;
using System;
using System.Collections.Generic;
using System.Web.Mvc;


namespace GoodBurger.Controllers
{
    [AllowAnonymous]
    public class RegisterController : Controller
    {
        private UserDal DalUser = new UserDal();
        private TownDal DalTown = new TownDal();

        /// <summary>
        /// Get index view
        /// </summary>
        /// <returns>View</returns>
        // GET: Register
        public ActionResult Index()
        {
            List<TownEntity> AllTown = DalTown.GetAllTown();
            ViewData["AllTown"] = AllTown;
            return View("Index");
        }

        /// <summary>
        /// Check and add new user in data base
        /// </summary>
        /// <param name="dataForm">FormCollection</param>
        /// <returns>View</returns>
        //Post : Register
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Register(FormCollection dataForm)
        {
           try
           {  
                    string firstName = dataForm["FirstName"];
                    string lasttName = dataForm["LastName"];
                    int phone = int.Parse(dataForm["Phone"]);
                    string email = dataForm["Email"];
                    string password = PasswordHandler.MD5Hash(dataForm["Password"]);
                    string address = dataForm["Email"];
                    int postalCode = int.Parse(dataForm["PostalCode"]);
                    long townId = long.Parse(dataForm["Town"]);

                if (!DalUser.RegisterUser(firstName, lasttName, phone, email, password, address, postalCode, townId).Equals(0))
                    {
                        return RedirectToAction("Index", "Login");
                    }
 
            } catch (Exception e) {
               ModelState.AddModelError("Error in register action", e.Message);
            }

            return View("Index");
        }

    }
}