﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Security;

namespace GoodBurger.Controllers
{
    public class LogoutController : Controller
    {
        /// <summary>
        /// Logout anu user( admin director and user) 
        /// delete all cookie and session 
        /// </summary>
        /// <returns>RedirectToAction</returns>
        // GET: Logout
        public ActionResult Index()
        {
            if (HttpContext.User.Identity.IsAuthenticated)
                FormsAuthentication.SignOut();

            if (Session["Role"] != null)
                Session.RemoveAll(); 

            return RedirectToAction("Index", "Home");

        }
    }
}