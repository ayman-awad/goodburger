﻿using GoodBurger.Classes;
using GoodBurger.Models.ContextDB;
using GoodBurger.Models.DAL;
using GoodBurger.Models.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;


namespace GoodBurger.Controllers
{
    [Authorize]
    public class UserController : Controller
    {
        private UserEntity user = new UserEntity();
        private UserDal DalUser = new UserDal();
        private TagDal DalTag = new TagDal();
        private RestaurantDal DalRestaurant = new RestaurantDal();
        private TagEntity Tag = new TagEntity();
        private RestaurantEntity Restaurant = new RestaurantEntity();
        private BurgerEntity MyBurger = new BurgerEntity();
        private BurgerDal DalBurger = new BurgerDal();

        private List<RestaurantEntity> RestaurantsWithTag = new List<RestaurantEntity>();
        // GET: User

        /// <summary>
        /// Get index user
        /// </summary>
        /// <returns>View</returns>
        [HttpGet]
        public ActionResult Index()
        {
          
            if (HttpContext.User.Identity.IsAuthenticated)
            {
                string CurrentCookie = HttpContext.User.Identity.Name;
                long idUser = long.Parse(CurrentCookie);
                user = DalUser.GetUserById(idUser);

                List<RestaurantEntity> Restaurants = DalRestaurant.GetAllRestaurant();
                ViewData["Restaurants"] = Restaurants;

                List<TagEntity> Tags = DalTag.GetAllTag();
                ViewData["Tags"] = Tags;

                List<BurgerLikedUserEntity> BurgerLiked = new List<BurgerLikedUserEntity>();
                using (DataBaseContext ctx = new DataBaseContext())
                {
                    BurgerLiked = ctx.BurgersLikedUsers.Where(r => r.UserId == idUser).ToList();
                    ViewData["BurgerLiked"] = BurgerLiked;

                    List<BurgerEntity> Burger = new List<BurgerEntity>();

                    foreach(BurgerLikedUserEntity BurgerLike in BurgerLiked)
                    {
                        Burger.Add(DalBurger.GetBurgerById(BurgerLike.BurgerId));
                    }
                    ViewData["BurgerSuggestion"] = Burger;
                }

                return View(user); 
            }
            return RedirectToAction("Index", "Home");
        }

        /// <summary>
        /// Get from to update profil
        /// </summary>
        /// <returns>View</returns>
        // GET: User Profil
        [HttpGet]
        public ActionResult Profil()
        {

            if (HttpContext.User.Identity.IsAuthenticated)
            {
                string CurrentCookie = HttpContext.User.Identity.Name;
                long idUser = long.Parse(CurrentCookie);
                user = DalUser.GetUserById(idUser);
                return View(user);
            }


            return RedirectToAction("Index", "User");
            
        }

        /// <summary>
        /// User can delete sis profil from the data base
        /// </summary>
        /// <returns>View</returns>
        // DELEGTE: User Profil
        public ActionResult Delete()
        {
                 
            if (HttpContext.User.Identity.IsAuthenticated)
            {
                string CurrentCookie = HttpContext.User.Identity.Name;
                long idUser = long.Parse(CurrentCookie);
                user = DalUser.GetUserById(idUser);
                UserEntity userRemove = DalUser.GetUserById(user.UserID);
                DalUser.RemoveUser(userRemove);
                return View(user);
            }
            return RedirectToAction("Index", "Home");
        }

        /// <summary>
        /// Check and update user information
        /// </summary>
        /// <param name="dataForm">FormCollection</param>
        /// <returns>RedirectToAction</returns>
        // Post: Update Profil
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Update(FormCollection dataForm)
        {
            if (dataForm != null){

                        Boolean Isdifferent = false;
                        UserEntity user = new UserEntity();
                        string CurrentCookie = HttpContext.User.Identity.Name;
                        long idUser = long.Parse(CurrentCookie);
                        user = DalUser.GetUserById(idUser);

                        string firstName = dataForm["FirstName"];
                        string lasttName = dataForm["LastName"];
                        int phone = int.Parse(dataForm["Phone"]);
                        string email = dataForm["Email"];
                        string password = PasswordHandler.MD5Hash(dataForm["Password"]);
                        string address = dataForm["Address"];
                        int postalCode = int.Parse(dataForm["PostalCode"]);

                        if (user != null) {
                     
                            if (firstName != user.FirstName){
                                user.FirstName = firstName;
                                Isdifferent = true;
                            }
                            if (lasttName != user.LastName){
                                user.LastName = lasttName;
                                Isdifferent = true;
                            }
                            if (email != user.Email){
                                user.Email = email;
                                Isdifferent = true;
                            }
                            if (password != user.Password){
                                user.Password = password;
                                Isdifferent = true;
                            }
                            if (phone != user.Phone){
                                user.Phone = phone;
                                Isdifferent = true;
                            }
                            if (address != user.Address){
                                user.Address = address;
                                Isdifferent = true;
                            }

                            if (postalCode != user.PostalCode)
                            {
                                user.PostalCode = postalCode;
                                Isdifferent = true;
                            }

                            if (Isdifferent == true){
                                        DalUser.UpdateUser(user);
                                        return RedirectToAction("Profil", "User");
                            }

                    }
            }
            return RedirectToAction("Index", "User");
        }

        /// <summary>
        /// Get manager user view
        /// </summary>
        /// <returns>View</returns>
        [HttpGet]
        public ActionResult ManagerUser()
        {
            List<UserEntity> AllUsers = new List<UserEntity>();
            AllUsers = DalUser.GetAllUsers();
            ViewData["AllUsers"] = AllUsers;
            return View();
        }

        /// <summary>
        /// Remove use from the data bse for administrator
        /// </summary>
        /// <param name="iduser">long</param>
        /// <returns>RedirectToAction</returns>
        [HttpGet]
        public ActionResult Remove(long iduser)
        {
            UserEntity user = DalUser.GetUserById(iduser);
            DalUser.RemoveUser(user);
            return RedirectToAction("ManagerUser", "User");
        }

        /// <summary>
        /// Allow to block account user login
        /// </summary>
        /// <param name="blocked">bool</param>
        /// <param name="iduser">long</param>
        /// <returns>RedirectToAction</returns>
        [HttpGet]
        public ActionResult Block(bool blocked, long iduser)
        {
            if (blocked.Equals(true))
            {
                user = DalUser.GetUserById(iduser);
                user.AccountBlock = true;
                DalUser.UpdateUser(user);
            }
            return RedirectToAction("ManagerUser", "User");

        }

        /// <summary>
        /// Allow to unblock account user login
        /// </summary>
        /// <param name="blocked">bool</param>
        /// <param name="iduser">long</param>
        /// <returns>RedirectToAction</returns>
        [HttpGet]
        public ActionResult UnBlock(bool blocked, long iduser)
        {
            if (blocked.Equals(false))
            {
                user = DalUser.GetUserById(iduser);
                user.AccountBlock = false;
                DalUser.UpdateUser(user);
            }
            return RedirectToAction("ManagerUser", "User");
        }

     

    }
}