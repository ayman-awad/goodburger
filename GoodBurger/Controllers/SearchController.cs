﻿using GoodBurger.Models.ContextDB;
using GoodBurger.Models.DAL;
using GoodBurger.Models.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace GoodBurger.Controllers
{
    public class SearchController : Controller
    {

        private UserEntity user = new UserEntity();
        private UserDal DalUser = new UserDal();
        private TagDal DalTag = new TagDal();
        private RestaurantDal DalRestaurant = new RestaurantDal();
        private TagEntity Tag = new TagEntity();
        private RestaurantEntity Restaurant = new RestaurantEntity();
        private BurgerEntity MyBurger = new BurgerEntity();
        private BurgerDal DalBurger = new BurgerDal();

        private List<RestaurantEntity> RestaurantsWithTag = new List<RestaurantEntity>();

        /// <summary>
        /// Get view index 
        /// </summary>
        /// <returns>View</returns>
        public ActionResult Index()
        {
            if (TempData["ResultSearch"] != null)
            {

                ViewData["Result"] = TempData["ResultSearch"];
            }

            List<RestaurantEntity> Restaurants = DalRestaurant.GetAllRestaurant();
            ViewData["Restaurants"] = Restaurants;

            List<TagEntity> Tags = DalTag.GetAllTag();
            ViewData["Tags"] = Tags;

            return View();
        }

        /// <summary>
        /// Get restaurant concern by tag choose by the user
        /// </summary>
        /// <param name="dataForm">FormCollection</param>
        /// <returns>RedirectToAction</returns>
        [HttpPost]
        public ActionResult Search(FormCollection dataForm)
        {
            long TagId = long.Parse(dataForm["myTag"]);
            Tag = DalTag.GetTagById(TagId);
            List<long> BurgerId = new List<long>();

            using (DataBaseContext ContextDb = new DataBaseContext())
            {
                List<TagInBurgerEntity> TagBurger = ContextDb.TagsInBurgers.Where(r => r.TagId == TagId).ToList();
                foreach (TagInBurgerEntity Burger in TagBurger)
                {
                    BurgerId.Add(Burger.BurgerId);
                }

                foreach (long Burger in BurgerId)
                {
                    MyBurger = DalBurger.GetBurgerById(Burger);
                    RestaurantEntity Restaurant = DalRestaurant.GetRestaurantById(MyBurger.CurrentIdRestaurant);
                    RestaurantEntity InList = RestaurantsWithTag.Find(item => item.RestaurantId == Restaurant.RestaurantId);
                    if (InList == null)
                    {
                        RestaurantsWithTag.Add(DalRestaurant.GetRestaurantById(MyBurger.CurrentIdRestaurant));
                    }

                }
            }
            TempData["ResultSearch"] = RestaurantsWithTag;

            return RedirectToAction("Index");
        }

        /// <summary>
        /// Reset search user and get all restaurant
        /// </summary>
        /// <returns>RedirectToAction</returns>
        public ActionResult ResetSearch()
        {
            if (RestaurantsWithTag != null)
            {
                RestaurantsWithTag.Clear();
            }

            return RedirectToAction("Index", "Search");
        }
    }
}