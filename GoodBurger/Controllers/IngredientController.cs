﻿using GoodBurger.Models.DAL;
using GoodBurger.Models.Entities;
using System;
using System.Collections.Generic;

using System.Web.Mvc;

namespace GoodBurger.Controllers
{
    [Authorize]
    public class IngredientController : Controller
    {
        private IngredientEntity Ingredient = null;
        private IngredientDal Dal = new IngredientDal();
        private DirectorDal DalDirector = new DirectorDal();

        /// <summary>
        /// Get index view
        /// </summary>
        /// <returns>View</returns>
        // GET: Ingredient
        [HttpGet]
        public ActionResult Index()
        {
            string CurrentCookie = HttpContext.User.Identity.Name;
            long IdDirector = long.Parse(CurrentCookie);
            DirectorEntity Director = DalDirector.GetOwnerById(IdDirector);
            List<IngredientEntity> AllIngredients = Dal.GetAllIngredientByIdRestaurant(Director.CurrentRestaurantId);
            ViewData["AllIngredients"] = AllIngredients;
            return View();
        }

        /// <summary>
        /// Get form add ingredient
        /// </summary>
        /// <returns>View</returns>
        [HttpGet]
        // GET: Ingredient
        public ActionResult FormAddIngredient()
        {
            return View();
        }

        /// <summary>
        /// Get for mupdate ingredient
        /// </summary>
        /// <param name="IngredientId">long</param>
        /// <returns>View</returns>
        [HttpGet]
        // GET: Ingredient
        public ActionResult FormUpdateIngredient(long IngredientId)
        {
            Ingredient = Dal.GetIngredientById(IngredientId);
            return View(Ingredient);
        }

        /// <summary>
        /// Check and update information ingredient
        /// </summary>
        /// <param name="dataForm">FormCollection</param>
        /// <param name="IngredientId">long</param>
        /// <returns>RedirectToAction</returns>
        [HttpPost]
        [ValidateAntiForgeryToken]
        //POST: Ingredient
        public ActionResult Update(FormCollection dataForm, long IngredientId) {

            Ingredient = Dal.GetIngredientById(IngredientId);

            if (dataForm != null)
            {

                Boolean Isdifferent = false;
                string Name = dataForm["Name"];
                string TypeIngredient = dataForm["TypeIngredient"];
                int Quantity = int.Parse(dataForm["Quantity"]);

                if (Ingredient != null)
                {

                    if (Name != Ingredient.Name)
                    {
                        Ingredient.Name = Name;
                        Isdifferent = true;
                    }

                    if (TypeIngredient != Ingredient.TypeIngredient)
                    {
                        Ingredient.TypeIngredient = TypeIngredient;
                        Isdifferent = true;
                    }
                    if (Quantity != Ingredient.Quantity)
                    {
                        Ingredient.Quantity = Quantity;
                        Isdifferent = true;
                    }


                    if (Isdifferent == true)
                    {
                        Dal.Update(Ingredient);
                        
                    }

                }

            }
            return RedirectToAction("Index", "Ingredient");
        }

        /// <summary>
        /// Check and add ingredient
        /// </summary>
        /// <param name="dataForm">FormCollection</param>
        /// <returns>RedirectToAction</returns>
        [HttpPost]
        [ValidateAntiForgeryToken]
        //POST: Ingredient
        public ActionResult Add(FormCollection dataForm)
        {
            try
            {

                string CurrentCookie = HttpContext.User.Identity.Name;
                long IdDirector = long.Parse(CurrentCookie);
                
                string Name = dataForm["Name"];
                string TypeIngredient = dataForm["TypeIngredient"];
                int Quantity = int.Parse(dataForm["Quantity"]);

                Ingredient = new IngredientEntity(Name, TypeIngredient, Quantity, IdDirector,DalDirector.GetOwnerById(IdDirector).CurrentRestaurantId);
                if (Ingredient != null)
                {
                    long id = Dal.Add(Ingredient);
                }

            }
            catch (Exception e)
           {
                ModelState.AddModelError("Error in register action", e.Message);
           }

            return RedirectToAction("Index", "Ingredient");
        }

        /// <summary>
        /// Remove ingredient in data base
        /// </summary>
        /// <param name="IngredientId">long</param>
        /// <returns>RedirectToAction</returns>
        public ActionResult Remove(long IngredientId)
        {
            if (!IngredientId.Equals(0))
            {
                Ingredient  = Dal.GetIngredientById(IngredientId);
                Dal.Remove(Ingredient);
                
            }
            return RedirectToAction("Index", "Ingredient");
        }
    }
}